#pragma once

#include "I3/MessageDispatcher.h"
#include "I3v0Demo/I3v0Dumper.h"

namespace i3
{

class I3v0Preset
{
public:
    static bool makeSSSP_CBR(MessageDispatcher& dispatcher, I3v0Dumper& dumper);
    static bool makeSSSP(MessageDispatcher& dispatcher, I3v0Dumper& dumper);
    static bool makeSSMP(MessageDispatcher& dispatcher, I3v0Dumper& dumper);

    static bool makeSSSP_Corrupted(MessageDispatcher& dispatcher, I3v0Dumper& dumper);

protected:
    static bool makeCommon(MessageDispatcher& dispatcher);
};

} // namespace i3
