#include "I3v0Demo/I3v0Dumper.h"
#include "I3v0Demo/I3v0Preset.h"

#include "I3/Common/RateBalancer.h"

#include "I3/Common/Profile.h"

using namespace std::chrono;
using namespace std::chrono_literals;

namespace
{

struct ByteRepr
{
public:
    const char* operator()(uint64_t bytes)
    {
        constexpr const char* kSuffixes[]
        {
            "B",
            "KB",
            "MB",
            "GB",
            "TB",
            "PB"
        };

        int32_t order = 0;
        int32_t ret = 0;

        do
        {
            if (bytes % 1024 != 0 || bytes == 0)
            {
                memcpy_s(m_Temp, sizeof(m_Temp), m_Buff, ret); // avoid pointer overlapping
                ret = sprintf_s(m_Buff, sizeof(m_Buff), "%llu %s %.*s", bytes % 1024, kSuffixes[order], ret, m_Temp);
            }

            ++order;

            bytes /= 1024;
        } while (bytes != 0);

        m_Buff[ret - 1] = '\0';

        return m_Buff;
    }

private:
    constexpr static size_t kCapacity{2048};
    char m_Buff[kCapacity]{};
    char m_Temp[kCapacity];
};

void PrintStatistics(const i3::MessageDispatcher::Statistics& stats, size_t bufferSize)
{
    i3ProfileFunction;

    const int32_t msgTotal = stats.MessagePassed + stats.MessageDropped;
    const float msgPassRate = msgTotal > 0 ? stats.MessagePassed / static_cast<float>(msgTotal) : 0.0f;

    static ByteRepr reprBitrateIn{};
    static ByteRepr reprBitrateOut{};
    static ByteRepr reprBitrateRead{};
    static ByteRepr reprBufferSize{};

    printf("bitrate in     %s\n"
           "bitrate out    %s\n"
           "bitrate read   %s\n"
           "data sources   %zu\n"
           "msg passed     %i\n"
           "msg dropped    %i\n"
           "msg refused    %i\n"
           "msg pass rate  %i%%\n"
           "buffer         %s\n"
           "\n",
           reprBitrateIn(stats.bitrateIn() / 8),
           reprBitrateOut(stats.bitrateOut() / 8),
           reprBitrateRead(stats.bitrateRead() / 8),
           stats.DataSources.size(),
           stats.MessagePassed,
           stats.MessageDropped,
           stats.MessageRefused,
           static_cast<int32_t>(msgPassRate * 100),
           reprBufferSize(bufferSize));
}

const std::string kDefaultPresetName{"sssp"};

} // namespace

bool MakePreset(i3::MessageDispatcher& dispatcher, i3::I3v0Dumper& dumper, const std::string& name)
{
    if (name == "ssmp")
    {
        return i3::I3v0Preset::makeSSMP(dispatcher, dumper);
    }
    else if (name == "sssp_corrupted")
    {
        return i3::I3v0Preset::makeSSSP_Corrupted(dispatcher, dumper);
    }
    else if (name == "sssp_cbr")
    {
        return i3::I3v0Preset::makeSSSP_CBR(dispatcher, dumper);
    }
    else if (name == kDefaultPresetName)
    {
        return i3::I3v0Preset::makeSSSP(dispatcher, dumper);
    }

    i3LogError("Wrong preset name! The provided name=`%s` isn't expected!", name.c_str());

    return false;
}

bool Run(float readRate, const std::string& presetName)
{
    i3::I3v0Dumper dumper{};

    i3::MessageDispatcher dispatcher{};
    if (!MakePreset(dispatcher, dumper, presetName))
    {
        i3LogError("Failed to create preset!");
        return false;
    }

    i3LogInfo("Stream has started (preset=`%s`)...", presetName.c_str());

    using Clock_t = std::chrono::high_resolution_clock;

    auto streamStartedAt = Clock_t::now();

    std::optional<i3::RateBalancer> dumpRate{};
    dumpRate = i3::RateBalancer(readRate);

    auto statsPrintedAt = Clock_t::now();
    while (true)
    {
        auto now = Clock_t::now();
        if (now - statsPrintedAt >= 1s)
        {
            PrintStatistics(dispatcher.statistics(), dumper.statistics().ReadBufferSize);
            statsPrintedAt = now;
        }

        if (dumpRate.has_value() && dumpRate->skip())
        {
            continue;
        }

        if (!dumper.dump())
        {
            break;
        }
    }

    auto elapsedTotal = duration_cast<milliseconds>(Clock_t::now() - streamStartedAt).count();
    i3LogInfo("Stream has stopped... Took `%lli` ms", elapsedTotal);

    return true;
}

int main(int argc, const char* argv[])
{
    i3ProfileThread("Main");
    i3ProfileEnable;
    i3ProfileListenStart;

    std::string presetName{kDefaultPresetName};
    presetName = "ssmp";
    if (argc >= 2)
    {
        presetName = argv[1];

        i3LogInfo("Picking the preset name=`%s` passed via command line arguments", presetName.c_str());
    }

    constexpr float kReadRate{200.0f};
    const bool ok = Run(kReadRate, presetName);
    if (!ok)
    {
        i3LogError("Failed to run Dumper!");
        return -1;
    }

    return 0;
}
