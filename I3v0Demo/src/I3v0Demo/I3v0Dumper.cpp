#include "I3v0Demo/I3v0Dumper.h"

#include "I3/Common/Log.h"
#include "I3/Common/Profile.h"

#include <chrono>

using namespace std::chrono;
using namespace std::chrono_literals;

namespace
{

constexpr auto kNoMessageTimeout{5s};
constexpr auto kGrowthFactor{1.2f};
constexpr size_t kMinSpace{4096};
constexpr size_t kMaxSpace{100 * 1024 * 1024};

} // namespace

namespace i3
{

bool I3v0Dumper::init(MessageDispatcher& dispatcher, I3v0Dumper::Config&& cfg)
{
    auto sourceStatus = dispatcher.addSource<DataSourceMessageI3v0>(i3::crc32(cfg.SourceID));
    if (!sourceStatus.isOk())
    {
        i3LogError("Failed to init I3v0Dumper! Failed to add source!");
        return false;
    }

    m_DataSource = *sourceStatus;

    m_File.open(cfg.Filename, std::ios::trunc | std::ios::binary);
    if (!m_File)
    {
        i3LogError("Failed to init I3v0Dumper! Can't open file `%s`!", cfg.Filename.c_str());
        return false;
    }

    m_Filename = cfg.Filename;

    m_ReadBuffer.resize(kMinSpace);

    return true;
}

bool I3v0Dumper::dump()
{
    i3ProfileFunction;

    types::bytespan_mutable dest{m_ReadBuffer.data(), m_ReadBuffer.size()};
    const size_t ret = m_DataSource->read(dest);

    auto now = clock_t::now();

    if (ret > 0)
    {
        m_LastMessageArrivedAt = now;
    }
    else if (now - m_LastMessageArrivedAt >= kNoMessageTimeout)
    {
        i3LogWarn("Finishing stream to `%s`. NoMessage timeout `%u` ms has exceeded!",
                  m_Filename.c_str(),
                  duration_cast<milliseconds>(kNoMessageTimeout).count());
        return false;
    }

    i3ProfileBlock("WRITE TO FILE");
    m_File.write(reinterpret_cast<char*>(dest.data()), ret);
    i3ProfileBlockEnd;

    const size_t byterateIn = m_DataSource->bitrateIn() / 8;
    const size_t byterateOut = m_DataSource->bitrateOut() / 8;
    const size_t byterateRead = m_DataSource->bitrateRead() / 8;
    const size_t byterate = std::max({byterateIn, byterateOut, byterateRead});

    size_t adaptedSize = static_cast<size_t>(byterate * kGrowthFactor);
    adaptedSize = std::clamp(adaptedSize, kMinSpace, kMaxSpace);

    m_ReadBuffer.resize(adaptedSize);
    m_Statistics.ReadBufferSize = m_ReadBuffer.size();

    return true;
}

I3v0Dumper::Statistics I3v0Dumper::statistics() const
{
    return m_Statistics;
}

} // namespace i3
