#pragma once

#include "I3/MessageDispatcher.h"
#include "I3v0/DataSource/DataSourceMessageI3v0.h"

#include <vector>
#include <fstream>

namespace i3
{

class I3v0Dumper
{
public:
    using clock_t = std::chrono::high_resolution_clock;

    struct Config
    {
        std::string Filename{};
        decltype(MessageI3v0_Header::SourceID) SourceID{};
    };

    struct Statistics
    {
        size_t ReadBufferSize{};
    };

public:
    bool init(MessageDispatcher& dispatcher, Config&& cfg);

    bool dump();

    Statistics statistics() const;

protected:
    std::shared_ptr<DataSourceMessageI3v0> m_DataSource{};
    std::vector<uint8_t> m_ReadBuffer{};
    std::ofstream m_File{};
    std::string m_Filename{};
    Statistics m_Statistics{};

    clock_t::time_point m_LastMessageArrivedAt{clock_t::now()};
};

} // namespace i3
