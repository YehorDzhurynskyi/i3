#pragma once

#include "I3v0Demo/I3v0Preset.h"

#include "I3v0Demo/I3v0Dumper.h"

#include "I3/Metric/MetricGeneratorCBR.h"
#include "I3/Metric/MetricGeneratorVBR.h"

#include "I3v0/DataProvider/DataProviderGeneratorI3v0.h"

using namespace i3::literals;

using namespace std::chrono;
using namespace std::chrono_literals;

namespace i3
{

template<typename T>
using DistributionWeights = DataProviderGeneratorI3v0::Config::DistributionWeights<T>;

bool I3v0Preset::makeSSSP(MessageDispatcher& dispatcher, I3v0Dumper& dumper)
{
    if (!makeCommon(dispatcher))
    {
        return false;
    }

    constexpr decltype(MessageI3v0_Header::SourceID) kSourceID{42};

    {
        DataProviderGeneratorI3v0::Config cfg{};
        cfg.MetricBitrate = std::make_unique<MetricGeneratorVBR>(10_Mb_bitrate, 100_Mb_bitrate, 1500);
        cfg.SourceID = kSourceID;
        cfg.PayloadType = MessageI3v0_DataType::Int32;
        cfg.LiveTime = 10s;
        cfg.Balancer = RateBalancer(200.0f);

        dispatcher.addProvider<DataProviderGeneratorI3v0>(std::move(cfg));
    }

    I3v0Dumper::Config dumperCfg{};
    dumperCfg.Filename = "dump-sssp.bin";
    dumperCfg.SourceID = kSourceID;

    const bool ok = dumper.init(dispatcher, std::move(dumperCfg));
    if (!ok)
    {
        i3LogError("Can't init dumper! Aborting...");
        return false;
    }

    return true;
}

bool I3v0Preset::makeSSSP_CBR(MessageDispatcher& dispatcher, I3v0Dumper& dumper)
{
    if (!makeCommon(dispatcher))
    {
        return false;
    }

    constexpr decltype(MessageI3v0_Header::SourceID) kSourceID{42};

    {
        DataProviderGeneratorI3v0::Config cfg{};
        cfg.MetricBitrate = std::make_unique<MetricGeneratorCBR>(200_Mb_bitrate);
        cfg.SourceID = kSourceID;
        cfg.PayloadType = MessageI3v0_DataType::Int32;
        cfg.LiveTime = 10s;
        cfg.Balancer = RateBalancer(200.0f);

        dispatcher.addProvider<DataProviderGeneratorI3v0>(std::move(cfg));
    }

    I3v0Dumper::Config dumperCfg{};
    dumperCfg.Filename = "dump-sssp-cbr.bin";
    dumperCfg.SourceID = kSourceID;

    const bool ok = dumper.init(dispatcher, std::move(dumperCfg));
    if (!ok)
    {
        i3LogError("Can't init dumper! Aborting...");
        return false;
    }

    return true;
}

bool I3v0Preset::makeSSSP_Corrupted(MessageDispatcher& dispatcher, I3v0Dumper& dumper)
{
    if (!makeCommon(dispatcher))
    {
        return false;
    }

    constexpr decltype(MessageI3v0_Header::SourceID) kSourceID{42};

    {
        DistributionWeights<decltype(MessageI3v0_Header::PayloadType)> payloadType{};
        payloadType.add(0.25, MessageI3v0_DataType::UInt8);
        payloadType.add(0.25, MessageI3v0_DataType::Int16);
        payloadType.add(0.25, MessageI3v0_DataType::Int32);
        payloadType.add(0.25, MessageI3v0_DataType::Float32);
        payloadType.add(0.05, static_cast<MessageI3v0_DataType>(-3));

        DistributionWeights<decltype(MessageI3v0_Header::SourceID)> sourceID{};
        sourceID.add(0.995, kSourceID);
        sourceID.add(0.005, kSourceID + 1);

        DistributionWeights<decltype(MessageI3v0_Header::MagicWord)> magicWord{};
        magicWord.add(0.950, MessageI3v0_Header::kMagicWord);
        magicWord.add(0.050, {'F', 'A', 'K', 'E'});

        DataProviderGeneratorI3v0::Config cfg{};
        cfg.MetricBitrate = std::make_unique<MetricGeneratorCBR>(100_Mb_bitrate);
        cfg.MagicWord = magicWord;
        cfg.SourceID = sourceID;
        cfg.PayloadType = MessageI3v0_DataType::Int16;
        cfg.PayloadPattern = DataProviderGeneratorI3v0::Pattern::Ascending;
        cfg.LiveTime = 20s;
        cfg.Balancer = RateBalancer(400.0f);
        cfg.SequenceNumberSkipProbabilty = 0.0025f;

        dispatcher.addProvider<DataProviderGeneratorI3v0>(std::move(cfg));
    }

    I3v0Dumper::Config dumperCfg{};
    dumperCfg.Filename = "dump-sssp-corrupted.bin";
    dumperCfg.SourceID = kSourceID;

    const bool ok = dumper.init(dispatcher, std::move(dumperCfg));
    if (!ok)
    {
        i3LogError("Can't init dumper! Aborting...");
        return false;
    }

    return true;
}

bool I3v0Preset::makeSSMP(MessageDispatcher& dispatcher, I3v0Dumper& dumper)
{
    if (!makeCommon(dispatcher))
    {
        return false;
    }

    constexpr decltype(MessageI3v0_Header::SourceID) kSourceID{42};

    {
        DataProviderGeneratorI3v0::Config cfg{};
        cfg.MetricBitrate = std::make_unique<MetricGeneratorVBR>(10_Mb_bitrate, 100_Mb_bitrate, 1500);
        cfg.SourceID = kSourceID;
        cfg.PayloadType = MessageI3v0_DataType::Int32;
        cfg.LiveTime = 20s;
        cfg.Balancer = RateBalancer(200.0f);

        dispatcher.addProvider<DataProviderGeneratorI3v0>(std::move(cfg));
    }

    {
        DataProviderGeneratorI3v0::Config cfg{};
        cfg.MetricBitrate = std::make_unique<MetricGeneratorCBR>(100_Mb_bitrate);
        cfg.SourceID = kSourceID;
        cfg.PayloadType = MessageI3v0_DataType::Float32;
        cfg.PayloadPattern = DataProviderGeneratorI3v0::Pattern::Ascending;
        cfg.LiveTime = 10s;
        cfg.Balancer = RateBalancer(400.0f);

        dispatcher.addProvider<DataProviderGeneratorI3v0>(std::move(cfg));
    }

    I3v0Dumper::Config dumperCfg{};
    dumperCfg.Filename = "dump-ssmp.bin";
    dumperCfg.SourceID = kSourceID;

    const bool ok = dumper.init(dispatcher, std::move(dumperCfg));
    if (!ok)
    {
        i3LogError("Can't init dumper! Aborting...");
        return false;
    }

    return true;
}

bool I3v0Preset::makeCommon(MessageDispatcher& dispatcher)
{
    auto protocolStatus = dispatcher.addProtocol<MessageI3v0_Header>();
    if (!protocolStatus.isOk())
    {
        i3LogError("Failed to add protocol!");
        return false;
    }

    return true;
}

} // namespace i3
