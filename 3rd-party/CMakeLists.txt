if (NOT WIN32)
    message(FATAL_ERROR "There are no pre-built 3rd-party dependencies for non-Windows platforms!")
endif()

find_package(GTest REQUIRED
    PATHS
        ${CMAKE_CURRENT_SOURCE_DIR}/GTest

    # Prevents global lookup, but instead uses the locally provided package
    NO_DEFAULT_PATH
    NO_CMAKE_FIND_ROOT_PATH
)

find_package(easy_profiler REQUIRED
    PATHS
        ${CMAKE_CURRENT_SOURCE_DIR}/easy_profiler

    # Prevents global lookup, but instead uses the locally provided package
    NO_DEFAULT_PATH
    NO_CMAKE_FIND_ROOT_PATH
)
