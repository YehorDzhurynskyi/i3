#pragma once

#include "I3/DataSource/DataSource.h"
#include "I3/DataProvider/DataProvider.h"

#include "I3/Message/Protocol.h"

#include <memory>

namespace i3
{

class MessageDispatcher
{
public:
    enum class ErrorCode
    {
        OK = 0,

        ProtocolDuplication,
        ProtocolUnrecognized,
        ProtocolBroken,

        SourceDuplication,
        SourceNotFound,

        MessageRefused,
    };

    struct Statistics
    {
        struct DataSource
        {
            crc32_t SourceID{};
            uint64_t BitrateIn{};
            uint64_t BitrateOut{};
            uint64_t BitrateRead{};
        };

        int32_t MessagePassed{};
        int32_t MessageDropped{};
        int32_t MessageRefused{};

        std::vector<DataSource> DataSources{};

        uint64_t bitrateIn() const;
        uint64_t bitrateOut() const;
        uint64_t bitrateRead() const;
    };

public:
    MessageDispatcher();
    MessageDispatcher(MessageDispatcher&& rhs);
    MessageDispatcher& operator=(MessageDispatcher&& rhs);
    ~MessageDispatcher(); // For PImpl

    Status<ErrorCode> dispatch(types::bytespan message);

    template<typename MsgHeader>
    Status<ErrorCode> addProtocol();

    template<typename T, typename ... Args>
    Result<std::shared_ptr<T>, ErrorCode> addSource(Args ... args);

    template<typename T, typename ... Args>
    void addProvider(Args ... args);

    void addProvider(std::shared_ptr<DataProvider> dataProvider);

    Statistics statistics() const;

protected:
    Status<ErrorCode> addProtocol(std::shared_ptr<Protocol> protocol);
    Status<ErrorCode> addSource(std::shared_ptr<DataSource> dataSource);

protected:
    class Impl;
    std::unique_ptr<Impl> m_Impl{};
};

template<typename MsgHeader>
Status<MessageDispatcher::ErrorCode> MessageDispatcher::addProtocol()
{
    Protocol protocol = MsgHeader::makeProtocol();

    std::shared_ptr<Protocol> p = std::make_shared<Protocol>(std::move(protocol));

    return addProtocol(p);
}

template<typename T, typename ... Args>
Result<std::shared_ptr<T>, MessageDispatcher::ErrorCode>
MessageDispatcher::addSource(Args ... args)
{
    static_assert(std::is_base_of_v<DataSource, T>);

    std::shared_ptr<DataSource> dataSource = std::make_shared<T>(std::forward<Args>(args)...);

    const Status<ErrorCode> status = addSource(dataSource);
    if (!status.isOk())
    {
        return Result<std::shared_ptr<T>, ErrorCode>::make(status.code());
    }

    std::shared_ptr<T> ds = std::static_pointer_cast<T>(dataSource);

    return Result<std::shared_ptr<T>, ErrorCode>::make(ds);
}

template<typename T, typename ... Args>
void MessageDispatcher::addProvider(Args ... args)
{
    static_assert(std::is_base_of_v<DataProvider, T>);

    std::unique_ptr<DataProvider> dataProvider = std::make_unique<T>(std::forward<Args>(args)...);
    addProvider(std::move(dataProvider));
}

} // namespace i3
