#include "I3/DataSource/DataSource.h"

#include "I3/Common/Profile.h"

namespace i3
{

size_t DataSource::read(types::bytespan_mutable dest)
{
    i3ProfileFunction;

    const size_t ret = readInternal(dest);

    m_BitrateRead.add(ret);

    return ret;
}

bool DataSource::push(types::bytespan src)
{
    i3ProfileFunction;

    const bool pushed = pushInternal(src);

    if (pushed)
    {
        m_BitrateIn.add(src.size());
    }

    return pushed;
}

uint64_t DataSource::bitrateIn() const
{
    return m_BitrateIn.bitrate();
}

uint64_t DataSource::bitrateOut() const
{
    return m_BitrateOut.bitrate();
}

uint64_t DataSource::bitrateRead() const
{
    return m_BitrateRead.bitrate();
}

} // namespace i3
