#pragma once

#include <cstdint>

#include "I3/Common/CRC32.h"
#include "I3/Common/Result.h"
#include "I3/Common/Types.h"

#include "I3/Metric/MetricBitrate.h"

namespace i3
{

class DataSource
{
public:
    enum class ErrorCode
    {
        OK,

        Timeout,
        BrokenData,
    };

public:
    virtual ~DataSource() = default;

    size_t read(types::bytespan_mutable dest);
    bool push(types::bytespan src);

    virtual crc32_t getProtocolID() const = 0;
    virtual crc32_t getSourceID() const = 0;

    uint64_t bitrateIn() const;
    uint64_t bitrateOut() const;
    uint64_t bitrateRead() const;

protected:
    virtual size_t readInternal(types::bytespan_mutable dest) = 0;
    virtual bool pushInternal(types::bytespan src) = 0;

protected:
    mutable MetricBitrate m_BitrateIn{};
    mutable MetricBitrate m_BitrateOut{};
    mutable MetricBitrate m_BitrateRead{};
};

} // namespace i3
