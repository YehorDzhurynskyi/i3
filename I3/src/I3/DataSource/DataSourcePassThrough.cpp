#include "I3/DataSource/DataSourcePassThrough.h"

namespace i3
{

DataSourcePassThrough::DataSourcePassThrough(crc32_t protocolID, crc32_t sourceID)
    : m_ProtocolID{protocolID}
    , m_SourceID{sourceID}
{
}

size_t DataSourcePassThrough::readInternal(types::bytespan_mutable dest)
{
    size_t ret{};

    {
        std::lock_guard lk(m_DataMutex);

        ret = std::min(m_Data.size(), dest.size());

        std::copy_n(m_Data.begin(), ret, dest.begin());
        m_Data.erase(m_Data.begin(), std::next(m_Data.begin(), ret));
    }

    return ret;
}

bool DataSourcePassThrough::pushInternal(types::bytespan src)
{
    {
        std::lock_guard lk(m_DataMutex);
        m_Data.insert(m_Data.end(), src.begin(), src.end());
    }

    return true;
}

crc32_t DataSourcePassThrough::getProtocolID() const
{
    return m_ProtocolID;
}

crc32_t DataSourcePassThrough::getSourceID() const
{
    return m_SourceID;
}

} // namespace i3
