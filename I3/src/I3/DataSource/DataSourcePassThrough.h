#pragma once

#include "I3/DataSource/DataSource.h"

#include <vector>
#include <queue>
#include <mutex>

namespace i3
{

class DataSourcePassThrough : public DataSource
{
public:
    DataSourcePassThrough(crc32_t protocolID, crc32_t sourceID);

    crc32_t getProtocolID() const override;
    crc32_t getSourceID() const override;

protected:
    size_t readInternal(types::bytespan_mutable dest) override;
    bool pushInternal(types::bytespan src) override;

protected:
    crc32_t m_ProtocolID{};
    crc32_t m_SourceID{};

    std::deque<uint8_t> m_Data{};
    std::mutex m_DataMutex{};
};

} // namespace i3
