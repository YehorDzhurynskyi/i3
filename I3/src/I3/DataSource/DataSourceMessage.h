#pragma once

#include "I3/DataSource/DataSource.h"

#include "I3/Common/Log.h"
#include "I3/Common/Clock.h"
#include "I3/Common/QueueThreadSafe.h"
#include "I3/Common/CircularBuffer.h"

#include "I3/Message/Protocol.h"

#include <vector>
#include <array>
#include <deque>
#include <thread>
#include <condition_variable>

namespace i3
{

template<typename Msg>
class DataSourceMessage : public DataSource
{
public:
    using value_type = Msg;

    explicit DataSourceMessage(crc32_t sourceID);

    crc32_t getProtocolID() const override;
    crc32_t getSourceID() const override;

protected:
    bool pushInternal(types::bytespan src) override;

    virtual bool validateMessage(const Msg& msg) const = 0;

protected:
    crc32_t m_SourceID{};
    crc32_t m_ProtocolID{};

    QueueThreadSafe<Msg> m_QueueWrite{};

    std::mutex m_QueueWriteUpdatedMutex{};
    std::condition_variable m_QueueWriteUpdatedCV{};
};

template<typename Msg>
DataSourceMessage<Msg>::DataSourceMessage(crc32_t sourceID)
    : m_SourceID{sourceID}
{
    const auto& magicWord = Msg::header_type::kMagicWord;
    m_ProtocolID = crc32(magicWord.data(), magicWord.size());
}

template<typename Msg>
bool DataSourceMessage<Msg>::pushInternal(types::bytespan src)
{
    Msg msg{};

    const ProtocolStatus parsed = msg.parse(src);
    if (!parsed.isOk() || !validateMessage(msg))
    {
        return false;
    }

    if (!m_QueueWrite.push(std::move(msg)))
    {
        i3LogFatal("Couldn't push a Message! The circular buffer is full (size=`%zu`)!", m_QueueWrite.size());
        return false;
    }

    m_QueueWriteUpdatedCV.notify_one();

    return true;
}

template<typename Msg>
crc32_t DataSourceMessage<Msg>::getProtocolID() const
{
    return m_ProtocolID;
}

template<typename Msg>
crc32_t DataSourceMessage<Msg>::getSourceID() const
{
    return m_SourceID;
}

} // namespace i3
