#include "I3/Metric/MetricGeneratorVBR.h"

#include "I3/Common/Log.h"
#include "I3/Common/Clock.h"

#include <cmath>
#include <chrono>
#include <optional>
#include <algorithm>

namespace i3
{

class MetricGeneratorVBR::Impl
{
public:
    MetricGeneratorVBR::Impl(uint64_t bitrateMin, uint64_t bitrateMax, int32_t periodMSecs)
        : m_BitrateMin{bitrateMin}
        , m_BitrateMax{bitrateMax}
        , m_Freq{(2.0f * static_cast<float>(M_PI)) / static_cast<float>(periodMSecs)}
    {
        i3LogAssert(bitrateMin >= 0);
        i3LogAssert(bitrateMax > bitrateMin);
        i3LogAssert(periodMSecs > 0);
    }

    uint64_t bitrate()
    {
        using namespace std::chrono;

        const auto now = Clock::now();

        const uint64_t milliElapsed = duration_cast<milliseconds>(now - m_TimePointLast).count();

        const float bitrateNormalized = (std::cos(milliElapsed * m_Freq) + 1.0f) / 2.0f;
        const uint64_t bitrate = static_cast<uint64_t>((m_BitrateMax - m_BitrateMin) * bitrateNormalized) + m_BitrateMin;

        return bitrate;
    }

private:
    uint64_t m_BitrateMin{};
    uint64_t m_BitrateMax{};
    float m_Freq{};

    Clock::time_point m_TimePointLast{Clock::now()};
};

MetricGeneratorVBR::MetricGeneratorVBR(uint64_t bitrateMin, uint64_t bitrateMax, int32_t periodMSecs)
    : m_Impl{std::make_unique<MetricGeneratorVBR::Impl>(bitrateMin, bitrateMax, periodMSecs)}
{
}

MetricGeneratorVBR::~MetricGeneratorVBR() {}

uint64_t MetricGeneratorVBR::bitrate()
{
    i3LogAssert_NonNull(m_Impl);

    return m_Impl->bitrate();
}

} // namespace i3
