#pragma once

#include <cstdint>

namespace i3
{

namespace literals
{

constexpr uint64_t operator ""_Kb_bitrate(uint64_t v)
{
    return v * 1000 * 8;
}

constexpr uint64_t operator ""_Mb_bitrate(uint64_t v)
{
    return v * 1000_Kb_bitrate;
}

constexpr uint64_t operator ""_Gb_bitrate(uint64_t v)
{
    return v * 1000_Mb_bitrate;
}

} // namespace literals

// TODO: docstring
// TODO: get rid of I prefix to be consistent
class IMetricGeneratorBitrate
{
public:
    virtual ~IMetricGeneratorBitrate() = default;

    virtual uint64_t bitrate() = 0;
};

} // namespace i3
