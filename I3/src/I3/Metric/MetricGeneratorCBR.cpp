#include "I3/Metric/MetricGeneratorCBR.h"

#include "I3/Common/Log.h"

#include <algorithm>

namespace i3
{

MetricGeneratorCBR::MetricGeneratorCBR(uint64_t bitrate)
    : m_Bitrate{bitrate}
{
    i3LogAssert(bitrate >= 0);
}

uint64_t MetricGeneratorCBR::bitrate()
{
    return m_Bitrate;
}

} // namespace i3
