#pragma once

#include "I3/Common/Clock.h"

#include <cstdint>
#include <atomic>

namespace i3
{

class MetricBitrate
{
public:
    void add(size_t bytes);

    uint64_t bitrate();

private:
    Clock::time_point m_NowPeriod{Clock::now()};
    Clock::time_point m_LastUpdate{Clock::now()};

    std::atomic<size_t> m_Bytes{};
    std::atomic<uint64_t> m_Bitrate{};
};

} // namespace i3
