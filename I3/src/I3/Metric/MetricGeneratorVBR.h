#pragma once

#include "I3/Metric/IMetricGenerator.h"

#include <memory>

namespace i3
{

// TODO: docstring
class MetricGeneratorVBR : public IMetricGeneratorBitrate
{
public:
    MetricGeneratorVBR(uint64_t bitrateMin, uint64_t bitrateMax, int32_t periodMSecs);
    ~MetricGeneratorVBR(); // For PImpl

    uint64_t bitrate() override;

protected:
    class Impl;
    std::unique_ptr<Impl> m_Impl{};
};

} // namespace i3
