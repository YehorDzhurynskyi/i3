#include "I3/Metric/MetricBitrate.h"

#include "I3/Common/Log.h"

namespace i3
{

void MetricBitrate::add(size_t bytes)
{
    m_Bytes += bytes;

    m_LastUpdate = Clock::now();
}

uint64_t MetricBitrate::bitrate()
{
    using namespace std::chrono;
    using namespace std::chrono_literals;

    constexpr auto kPeriod{1s};

    auto now = Clock::now();
    if ((now - m_NowPeriod) >= kPeriod)
    {
        m_Bitrate = (now - m_LastUpdate) <= kPeriod ? m_Bytes * 8 : 0;

        m_NowPeriod = now;
        m_Bytes = 0;
    }

    return m_Bitrate;
}

} // namespace i3
