#pragma once

#include "I3/Metric/IMetricGenerator.h"

namespace i3
{

// TODO: docstring
class MetricGeneratorCBR : public IMetricGeneratorBitrate
{
public:
    explicit MetricGeneratorCBR(uint64_t bitrate);

    uint64_t bitrate() override;

protected:
    uint64_t m_Bitrate{};
};

} // namespace i3
