#include "I3/DataProvider/DataProviderGenerator.h"

#include "I3/Common/Log.h"

#include <vector>

namespace i3
{

DataProviderGenerator::DataProviderGenerator(std::unique_ptr<IMetricGeneratorBitrate>&& metricBitrate,
                                             std::optional<std::chrono::milliseconds> livetime,
                                             std::optional<RateBalancer> balancer)
    : m_MetricBitrate{std::move(metricBitrate)}
    , m_LiveTime{livetime}
    , m_RateBalancer{balancer}
{
}

Result<types::bytespan, DataProviderGenerator::ErrorCode> DataProviderGenerator::read()
{
    using namespace std::chrono;

    const auto now = Clock::now();

    if (m_LiveTime.has_value() && (now - m_StartedAt >= *m_LiveTime))
    {
        return Result<types::bytespan, ErrorCode>::make(ErrorCode::EOS);
    }

    if (m_RateBalancer.has_value() && m_RateBalancer->skip())
    {
        return Result<types::bytespan, ErrorCode>::make(ErrorCode::Skip);
    }

    const uint64_t nanoElapsed = duration_cast<nanoseconds>(now - m_TimePointLast).count();
    const float secElapsed = nanoElapsed / (1000.0f * 1000.0f * 1000.0f);

    m_TimePointLast = now;

    i3LogAssert_NonNull(m_MetricBitrate);
    const uint64_t bitrate = m_MetricBitrate->bitrate();
    const size_t size = static_cast<size_t>((bitrate / 8.0f) * secElapsed);

    generate(m_Chunk, size);

    if (m_Chunk.empty())
    {
        return Result<types::bytespan, ErrorCode>::make(ErrorCode::EmptyChunk);
    }

    const types::bytespan data{m_Chunk.data(), m_Chunk.size()};
    return Result<types::bytespan, ErrorCode>::make(data);
}

} // namespace i3
