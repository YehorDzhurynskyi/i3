#pragma once

#include "I3/DataProvider/DataProvider.h"

#include "I3/Common/Clock.h"
#include "I3/Common/RateBalancer.h"
#include "I3/Metric/IMetricGenerator.h"

#include <memory>
#include <optional>
#include <vector>

namespace i3
{

class DataProviderGenerator : public DataProvider
{
public:
    DataProviderGenerator(std::unique_ptr<IMetricGeneratorBitrate>&& metricBitrate,
                          std::optional<std::chrono::milliseconds> livetime,
                          std::optional<RateBalancer> balancer);

    Result<types::bytespan, ErrorCode> read() override;

protected:
    virtual void generate(std::vector<uint8_t>& dest, size_t size) = 0;

private:
    std::vector<uint8_t> m_Chunk{};
    std::unique_ptr<IMetricGeneratorBitrate> m_MetricBitrate{};

    Clock::time_point m_TimePointLast{Clock::now()};
    Clock::time_point m_StartedAt{Clock::now()};
    std::optional<std::chrono::milliseconds> m_LiveTime{};
    std::optional<RateBalancer> m_RateBalancer{};
};

} // namespace i3
