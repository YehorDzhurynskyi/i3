#pragma once

#include "I3/DataProvider/DataProvider.h"

#include <vector>
#include <list>
#include <mutex>

namespace i3
{

class DataProviderInMemory : public DataProvider
{
public:
    Result<types::bytespan, ErrorCode> read() override;

    void push(std::vector<uint8_t>&& chunk);

protected:
    std::list<std::vector<uint8_t>> m_Chunks{};
    int32_t m_ChunkNextIdx{0};
    std::mutex m_ChunksMutex{};
};

} // namespace i3
