#pragma once

#include "I3/Common/Result.h"
#include "I3/Common/Types.h"

#include <cstdint>

namespace i3
{

class DataProvider
{
public:
    enum class ErrorCode
    {
        OK,

        EOS,
        Skip,
        EmptyChunk,
    };

public:
    virtual ~DataProvider() = default;

    virtual Result<types::bytespan, ErrorCode> read() = 0;
};

} // namespace i3
