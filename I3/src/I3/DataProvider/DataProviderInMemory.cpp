#include "I3/DataProvider/DataProviderInMemory.h"

namespace i3
{

Result<types::bytespan, DataProviderInMemory::ErrorCode> DataProviderInMemory::read()
{
    std::lock_guard lk(m_ChunksMutex);

    if (m_ChunkNextIdx >= m_Chunks.size())
    {
        return Result<types::bytespan, ErrorCode>::make(ErrorCode::EOS);
    }

    const std::vector<uint8_t>& chunk = *std::next(m_Chunks.begin(), m_ChunkNextIdx++);
    const types::bytespan data{chunk.data(), chunk.size()};

    return Result<types::bytespan, ErrorCode>::make(data);
}

void DataProviderInMemory::push(std::vector<uint8_t>&& chunk)
{
    std::lock_guard lk(m_ChunksMutex);
    m_Chunks.push_back(std::move(chunk));
}

} // namespace i3
