#pragma once

#include <cstdint>
#include <cstdlib>

namespace i3
{

enum class LogLevel
{
    Mute,
    Trace,
    Debug,
    Info,
    Warn,
    Error
};

void log(LogLevel level,
        const char* file,
        const char* function,
        uint32_t line,
        const char* format,
        ...);

} // namespace i3

#define i3BreakPoint                                   __debugbreak()

#ifdef _DEBUG
#define i3Abort                                        i3BreakPoint
#else
#define i3Abort                                        abort()
#endif

#define i3Log(level, format, ...)                      i3::log(level, __FILE__, __FUNCTION__, __LINE__, format, ##__VA_ARGS__)
#define i3LogTrace(format, ...)                        i3Log(i3::LogLevel::Trace, format, ##__VA_ARGS__)
#define i3LogDebug(format, ...)                        i3Log(i3::LogLevel::Debug, format, ##__VA_ARGS__)
#define i3LogInfo(format, ...)                         i3Log(i3::LogLevel::Info, format, ##__VA_ARGS__)
#define i3LogWarn(format, ...)                         i3Log(i3::LogLevel::Warn, format, ##__VA_ARGS__)
#define i3LogError(format, ...)                        do { i3Log(i3::LogLevel::Error, format, ##__VA_ARGS__); } while (0)
#define i3LogFatal(format, ...)                        do { i3LogError(format, ##__VA_ARGS__); i3Abort; } while (0)

#define i3LogEx(level, file, fn, line, format, ...)    i3::log(level, file, fn, line, format, ##__VA_ARGS__)
#define i3LogTraceEx(file, fn, line, format, ...)      i3LogEx(i3::LogLevel::Trace, file, fn, line, format, ##__VA_ARGS__)
#define i3LogDebugEx(file, fn, line, format, ...)      i3LogEx(i3::LogLevel::Debug, file, fn, line, format, ##__VA_ARGS__)
#define i3LogInfoEx(file, fn, line, format, ...)       i3LogEx(i3::LogLevel::Info, file, fn, line, format, ##__VA_ARGS__)
#define i3LogWarnEx(file, fn, line, format, ...)       i3LogEx(i3::LogLevel::Warn, file, fn, line, format, ##__VA_ARGS__)
#define i3LogErrorEx(file, fn, line, format, ...)      do { i3LogEx(i3::LogLevel::Error, file, fn, line, format, ##__VA_ARGS__); } while (0)
#define i3LogFatalEx(file, fn, line, format, ...)      do { i3LogErrorEx(file, fn, line, format, ##__VA_ARGS__); i3Abort; } while (0)

#ifdef _DEBUG
#define i3LogAssert(condition)                         do { if (!(condition))  { i3LogFatal("%s", "ASSERTION FAILED - "#condition); } } while (0)
#define i3LogAssertEx(file, fn, line, condition)       do { if (!(condition)) { i3LogFatalEx(file, fn, line, "%s", "ASSERTION FAILED - "#condition); } } while (0)
#else
#define i3LogAssert(condition)                         do { if (!(condition))  { i3LogError("%s", "ASSERTION FAILED - "#condition); } } while (0)
#define i3LogAssertEx(file, fn, line, condition)       do { if (!(condition)) { i3LogErrorEx(file, fn, line, "%s", "ASSERTION FAILED - "#condition); } } while (0)
#endif

#define i3LogAssert_BreakPoint(x)                      do { if (!(x)) { i3BreakPoint; } } while (0)
#define i3LogAssert_NonNull(x)                         i3LogAssert(x != nullptr)
