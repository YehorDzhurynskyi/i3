#pragma once

#include "I3/Common/Clock.h"

namespace i3
{

class RateBalancer
{
public:
    RateBalancer() = default;
    explicit RateBalancer(float rate);

    bool skip();

private:
    float m_Rate{};
    Clock::time_point m_FiredAt{Clock::now()};
};

} // namespace i3
