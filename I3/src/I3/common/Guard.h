#pragma once

#include <functional>

namespace i3
{

class Guard
{
public:
    using Callback_t = std::function<void()>;

public:
    explicit Guard(Callback_t cb)
        : m_Callback{cb}
    {
    }

    ~Guard()
    {
        if (m_Callback)
        {
            m_Callback();
        }
    }

    void reset()
    {
        m_Callback = nullptr;
    }

protected:
    Callback_t m_Callback{};
};

} // namespace i3
