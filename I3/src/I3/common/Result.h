#pragma once

#include "I3/Common/Log.h"

namespace i3
{

template<typename V, typename E>
struct Result
{
    static Result<V, E> make(V value)
    {
        Result<V, E> result{};
        result.m_Value = value;

        return result;
    }

    static Result<V, E> make(E code)
    {
        Result<V, E> result{};
        result.m_ErrCode = code;

        return result;
    }

    bool isOk() const
    {
        return m_ErrCode == E{};
    }

    E code() const
    {
        return m_ErrCode;
    }

    const V& operator*() const
    {
        i3LogAssert(isOk());

        return m_Value;
    }

    V& operator*()
    {
        i3LogAssert(isOk());

        return m_Value;
    }

    const V* operator->() const
    {
        i3LogAssert(isOk());

        return &m_Value;
    }

    V* operator->()
    {
        i3LogAssert(isOk());

        return &m_Value;
    }

private:
    Result() = default;

    V m_Value{};
    E m_ErrCode{};
};

template<typename E>
struct Status
{
    static Status ok()
    {
        return make(E{});
    }

    static Status make(E code)
    {
        Status result{};
        result.m_ErrCode = code;

        return result;
    }

    bool isOk() const
    {
        return m_ErrCode == E{};
    }

    E code() const
    {
        return m_ErrCode;
    }

private:
    E m_ErrCode{};
};

} // namespace i3
