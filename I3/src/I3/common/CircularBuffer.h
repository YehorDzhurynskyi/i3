#pragma once

#include <vector>
#include <atomic>
#include <cstdint>

namespace i3
{

template<typename T>
class CircularBuffer
{
public:
    explicit CircularBuffer(size_t length);

    CircularBuffer(const CircularBuffer&) = delete;
    CircularBuffer& operator=(const CircularBuffer&) = delete;

    bool empty() const;
    bool full() const;
    size_t capacity() const;
    size_t size() const;

    bool push(const T& el);
    bool push(T&& el);

    void front(T& el) const;
    void pop();
    void clear();

protected:
    void advance();
    void retreat();

    std::atomic<int32_t> m_IndexWrite{0};
    std::atomic<int32_t> m_IndexRead{0};

    std::atomic<bool> m_IsFull{false};
    const size_t m_Length;

    std::vector<T> m_Buffer{};
};

template<typename T>
CircularBuffer<T>::CircularBuffer(size_t length)
    : m_Length(length)
{
    m_Buffer.resize(m_Length);
}

template<typename T>
bool CircularBuffer<T>::empty() const
{
    return (m_IndexWrite == m_IndexRead) && !m_IsFull;
}

template<typename T>
bool CircularBuffer<T>::full() const
{
    return m_IsFull || !capacity();
}

template<typename T>
size_t CircularBuffer<T>::capacity() const
{
    return m_Buffer.size();
}

template<typename T>
size_t CircularBuffer<T>::size() const
{
    const int32_t w = m_IndexWrite.load();
    const int32_t r = m_IndexRead.load();

    if (!full())
    {
        size_t max_size = capacity();
        return (max_size + w - r) % max_size;
    }

    return capacity();
}

template<typename T>
void CircularBuffer<T>::advance()
{
    int32_t w = m_IndexWrite.load();
    w = (w + 1) % capacity();

    m_IsFull = (w == m_IndexRead.load());
    m_IndexWrite = w;
}

template<typename T>
void CircularBuffer<T>::retreat()
{
    m_IsFull = false;
    m_IndexRead = (m_IndexRead.load() + 1) % capacity();
}

template<typename T>
void CircularBuffer<T>::front(T& el) const
{
    el = m_Buffer[m_IndexRead];
}

template<typename T>
bool CircularBuffer<T>::push(const T& el)
{
    if (full())
    {
        return false;
    }

    m_Buffer[m_IndexWrite] = el;
    advance();

    return true;
}

template<typename T>
bool CircularBuffer<T>::push(T&& el)
{
    if (full())
    {
        return false;
    }

    m_Buffer[m_IndexWrite] = std::move(el);
    advance();

    return true;
}

template<typename T>
void CircularBuffer<T>::pop()
{
    if (empty())
    {
        return;
    }

    retreat();
}

template<typename T>
void CircularBuffer<T>::clear()
{
    m_IsFull = false;
    m_IndexRead = m_IndexWrite.load();
}

} // namespace i3
