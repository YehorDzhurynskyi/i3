#include "I3/Common/RateBalancer.h"

using namespace std::chrono;

namespace i3
{

RateBalancer::RateBalancer(float rate)
    : m_Rate{rate}
{
}

bool RateBalancer::skip()
{
    const float periodSecs = 1.0f / m_Rate;

    const auto now = Clock::now();

    const uint64_t nanoElapsed = duration_cast<nanoseconds>(now - m_FiredAt).count();
    const float secElapsed = nanoElapsed / (1000.0f * 1000.0f * 1000.0f);

    const bool skip = (secElapsed < periodSecs);
    if (!skip)
    {
        m_FiredAt = now;
    }

    return skip;
}

} // namespace i3
