#include "I3/Common/Log.h"

#include <cstdarg>
#include <chrono>
#include <thread>
#include <atomic>
#include <cstdint>

#define ARRLEN(x) (sizeof(x) / sizeof((x)[0])

namespace
{

constexpr int32_t kLogCapacity{4096};

const char* LogLevelName(i3::LogLevel level)
{
    switch (level)
    {
    case i3::LogLevel::Trace: return "TRACE";
    case i3::LogLevel::Debug: return "DEBUG";
    case i3::LogLevel::Info: return "INFO";
    case i3::LogLevel::Warn: return "WARN";
    case i3::LogLevel::Error: return "ERROR";
    }

    return "???";
}

std::atomic<uint32_t> g_ThreadCount{0};

} // namespace

namespace i3
{

void log(LogLevel level,
         const char* file,
         const char* function,
         uint32_t line,
         const char* format,
         ...)
{
    static thread_local char data[kLogCapacity];
    const static thread_local uint32_t threadID{g_ThreadCount++};

    if (level == LogLevel::Mute)
    {
        return;
    }

    // calling now() before any processing to avoid user call latency
    const auto now = std::chrono::system_clock::now();

    va_list va{};
    va_start(va, format);

    int32_t len = 0;
    if ((len = vsnprintf(data, kLogCapacity, format, va)) >= kLogCapacity)
    {
        i3LogErrorEx(file, function, line, "Log message size=`%i` exceeds the message buffer capacity=`%zu`!",
                     len,
                     kLogCapacity);

        len = kLogCapacity;

        data[kLogCapacity - 1] = '.';
        data[kLogCapacity - 2] = '.';
        data[kLogCapacity - 3] = '.';
    }

    va_end(va);

    const int64_t sysTimeMS = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()).count();

    printf("[%lli sys ms][thread 0x%X] %s - `%s()` [line: %u]: [%s] %.*s\n",
           sysTimeMS,
           threadID,
           file,
           function,
           line,
           LogLevelName(level),
           len,
           data);
}

} // namespace i3
