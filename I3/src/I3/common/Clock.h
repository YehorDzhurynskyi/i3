#pragma once

#include <chrono>

#ifdef I3_BUILD_SIMULATION

namespace i3
{

struct ClockManualSteady
{
    using rep = long long;
    using period = std::milli;
    using duration = std::chrono::milliseconds;
    using time_point = std::chrono::time_point<ClockManualSteady>;
    static constexpr bool is_steady = true;

    static time_point now();

    static void reset();
    static void advance(const duration& dt);
};

using Clock = ClockManualSteady;

} // namespace i3

#else // I3_BUILD_SIMULATION

namespace i3
{

using Clock = std::chrono::high_resolution_clock;
static_assert(Clock::is_steady);

} // namespace i3
#endif // I3_BUILD_SIMULATION
