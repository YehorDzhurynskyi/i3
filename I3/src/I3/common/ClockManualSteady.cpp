#include "I3/Common/Clock.h"

#include <mutex>

namespace
{

i3::ClockManualSteady::time_point g_Now{};
std::mutex g_ClockMutex;

} // namespace

namespace i3
{

ClockManualSteady::time_point ClockManualSteady::now()
{
    ClockManualSteady::time_point now{};

    {
        std::lock_guard lk(g_ClockMutex);
        now = g_Now;
    }

    return now;
}

void ClockManualSteady::reset()
{
    std::lock_guard lk(g_ClockMutex);

    g_Now = ClockManualSteady::time_point{};
}

void ClockManualSteady::advance(const ClockManualSteady::duration& dt)
{
    std::lock_guard lk(g_ClockMutex);

    g_Now += dt;
}

} // namespace i3
