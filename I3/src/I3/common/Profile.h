#pragma once

#ifdef I3_BUILD_PROFILE
#include <easy/profiler.h>

namespace i3
{

profiler::color_t ProfileColorOf(std::string name);

} // namespace i3

#define i3ProfileFunction              EASY_FUNCTION(i3::ProfileColorOf(__FUNCTION__))
#define i3ProfileEnable                EASY_PROFILER_ENABLE
#define i3ProfileBlock(NAME)           EASY_BLOCK(NAME, i3::ProfileColorOf(NAME))
#define i3ProfileBlockEnd              EASY_END_BLOCK
#define i3ProfileThread(NAME)          EASY_THREAD(NAME)
#define i3ProfileCaptureStart          EASY_START_CAPTURE
#define i3ProfileCaptureStop           EASY_STOP_CAPTURE
#define i3ProfileListenStart           profiler::startListen()
#define i3ProfileListenStop            profiler::stopListen()
#define i3ProfileListenStartEx(PORT)   profiler::startListen(PORT)
#define i3ProfileListenStopEx(PORT)    profiler::stopListen(PORT)
#define i3ProfileDump(FILENAME)        profiler::dumpBlocksToFile(FILENAME)
#else // I3_BUILD_PROFILE
#define i3ProfileFunction
#define i3ProfileEnable
#define i3ProfileBlock(NAME)
#define i3ProfileBlockEnd
#define i3ProfileThread(NAME)
#define i3ProfileCaptureStart
#define i3ProfileCaptureStop
#define i3ProfileListenStart
#define i3ProfileListenStop
#define i3ProfileListenStartEx(PORT)
#define i3ProfileListenStopEx(PORT)
#define i3ProfileDump(FILENAME)
#endif // I3_BUILD_PROFILE
