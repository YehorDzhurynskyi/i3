#include "I3/Common/Types.h"

#include "I3/Common/Log.h"

namespace
{

template<typename T>
T UnpackSigned(const uint8_t* data, size_t sizeOf)
{
    T packed{};

    for (int32_t i = static_cast<int32_t>(sizeOf) - 1; i >= 0; --i)
    {
        packed |= (static_cast<T>(data[i]) << (8 * i));
    }

    return packed;
}

template<typename T>
std::optional<size_t> UnpackUnsigned(const uint8_t* data, size_t sizeOf)
{
    const T packed = UnpackSigned<T>(data, sizeOf);

    if constexpr (std::is_signed_v<T>)
    {
        if (packed < 0)
        {
            return std::nullopt;
        }
    }

    return static_cast<size_t>(packed);
}

} // namespace

namespace i3
{

namespace types
{

size_t SizeOf(Type datatype)
{
    switch (datatype)
    {
    case Type::Int8:
    case Type::UInt8: return 1;
    case Type::Int16:
    case Type::UInt16: return 2;
    case Type::Int32:
    case Type::UInt32:
    case Type::Float32: return 4;
    case Type::Int64:
    case Type::UInt64:
    case Type::Float64: return 8;
    }

    i3LogError("Unexpected Type=`%i`!", static_cast<int32_t>(datatype));

    return 0;
}


int64_t UnpackSigned(const uint8_t* data, Type datatype)
{
    const size_t sizeOf = i3::types::SizeOf(datatype);

    uint64_t value{};
    switch (datatype)
    {
    case Int8: value = ::UnpackSigned<int8_t>(data, sizeOf); break;
    case UInt8: value = ::UnpackSigned<uint8_t>(data, sizeOf); break;
    case Int16: value = ::UnpackSigned<int16_t>(data, sizeOf); break;
    case UInt16: value = ::UnpackSigned<uint16_t>(data, sizeOf); break;
    case Int32: value = ::UnpackSigned<int32_t>(data, sizeOf); break;
    case UInt32: value = ::UnpackSigned<uint32_t>(data, sizeOf); break;
    case Int64: value = ::UnpackSigned<int64_t>(data, sizeOf); break;
    case UInt64: value = ::UnpackSigned<uint64_t>(data, sizeOf); break;
    default:
    {
        i3LogError("Unexpected Type=`%i`!", static_cast<int32_t>(datatype));
        break;
    }
    }

    return value;
}

std::optional<uint64_t> UnpackUnsigned(const uint8_t* data, Type datatype)
{
    const size_t sizeOf = i3::types::SizeOf(datatype);

    std::optional<uint64_t> value{};
    switch (datatype)
    {
    case Int8: value = ::UnpackUnsigned<int8_t>(data, sizeOf); break;
    case UInt8: value = ::UnpackUnsigned<uint8_t>(data, sizeOf); break;
    case Int16: value = ::UnpackUnsigned<int16_t>(data, sizeOf); break;
    case UInt16: value = ::UnpackUnsigned<uint16_t>(data, sizeOf); break;
    case Int32: value = ::UnpackUnsigned<int32_t>(data, sizeOf); break;
    case UInt32: value = ::UnpackUnsigned<uint32_t>(data, sizeOf); break;
    case Int64: value = ::UnpackUnsigned<int64_t>(data, sizeOf); break;
    case UInt64: value = ::UnpackUnsigned<uint64_t>(data, sizeOf); break;
    default:
    {
        i3LogError("Unexpected Type=`%i`!", static_cast<int32_t>(datatype));
        break;
    }
    }

    return value;
}

} // namespace types

} // namespace i3
