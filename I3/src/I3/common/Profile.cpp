#include "I3/Common/Profile.h"

#ifdef I3_BUILD_PROFILE
namespace i3
{

profiler::color_t ProfileColorOf(std::string name)
{
    const profiler::color_t kColors[] {
        profiler::colors::Amber,
        profiler::colors::Red,
        profiler::colors::Blue,
        profiler::colors::Magenta,
        profiler::colors::Pink,
        profiler::colors::Cyan,
        profiler::colors::Green,
        profiler::colors::Lime,
        profiler::colors::Brown800,
        profiler::colors::DeepOrange800,
        profiler::colors::DarkBlue,
        profiler::colors::DarkRed,
        profiler::colors::DarkGreen
    };

    const size_t hash = std::hash<std::string>()(name);
    return kColors[hash % (sizeof(kColors) / sizeof(kColors[0]))];
}

} // namespace i3
#endif // I3_BUILD_PROFILE
