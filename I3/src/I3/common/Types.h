#pragma once

#include <cstdint>
#include <optional>

namespace i3
{

namespace types
{

template<typename T, bool Mutable = false>
class span
{
public:
    using value_type = std::conditional_t<Mutable, T*, const T*>;

public:
    span() = default;
    span(value_type data, size_t size);

    value_type data() const;
    size_t size() const;

    value_type begin() const;
    value_type end() const;

    bool operator==(const span& rhs) const;
    bool operator!=(const span& rhs) const;

private:
    value_type m_Data{};
    size_t m_Size{};
};

template<typename T>
using span_mutable = span<T, true>;

using bytespan = span<uint8_t>;
using bytespan_mutable = span_mutable<uint8_t>;

template<typename T, bool Mutable>
span<T, Mutable>::span(typename span<T, Mutable>::value_type data, size_t size)
    : m_Data{data}
    , m_Size{size}
{
}

template<typename T, bool Mutable>
typename span<T, Mutable>::value_type span<T, Mutable>::data() const
{
    return m_Data;
}

template<typename T, bool Mutable>
size_t span<T, Mutable>::size() const
{
    return m_Size;
}

template<typename T, bool Mutable>
typename span<T, Mutable>::value_type span<T, Mutable>::begin() const
{
    return data();
}

template<typename T, bool Mutable>
typename span<T, Mutable>::value_type span<T, Mutable>::end() const
{
    return data() + size();
}

template<typename T, bool Mutable>
bool span<T, Mutable>::operator==(const span& rhs) const
{
    if (size() != rhs.size())
    {
        return false;
    }

    return std::equal(begin(), end(), rhs.begin(), rhs.end());
}

template<typename T, bool Mutable>
bool span<T, Mutable>::operator!=(const span& rhs) const
{
    return !(*this == rhs);
}

enum Type
{
    Int8,
    UInt8,

    Int16,
    UInt16,

    Int32,
    UInt32,

    Int64,
    UInt64,

    Float32,
    Float64,
};

size_t SizeOf(Type datatype);
int64_t UnpackSigned(const uint8_t* data, Type datatype);
std::optional<uint64_t> UnpackUnsigned(const uint8_t* data, Type datatype);

template<typename T, bool = std::is_enum_v<T>>
struct type_of
{
};

template<typename T>
struct type_of<T, true>
{
    constexpr static Type value{type_of<std::underlying_type_t<T>>::value};
};

template<typename T>
constexpr Type type_of_v = type_of<T>::value;

template<>
struct type_of<int8_t>
{
    constexpr static Type value{Type::Int8};
};

template<>
struct type_of<uint8_t>
{
    constexpr static Type value{Type::UInt8};
};

template<>
struct type_of<int16_t>
{
    constexpr static Type value{Type::Int16};
};

template<>
struct type_of<uint16_t>
{
    constexpr static Type value{Type::UInt16};
};

template<>
struct type_of<int32_t>
{
    constexpr static Type value{Type::Int32};
};

template<>
struct type_of<uint32_t>
{
    constexpr static Type value{Type::UInt32};
};

template<>
struct type_of<int64_t>
{
    constexpr static Type value{Type::Int64};
};

template<>
struct type_of<uint64_t>
{
    constexpr static Type value{Type::UInt64};
};

} // namespace types

} // namespace i3
