#pragma once

#include <queue>
#include <cstdint>
#include <mutex>

namespace i3
{

template<typename T>
class QueueThreadSafe
{
public:
    bool empty() const;
    size_t capacity() const;
    size_t size() const;

    bool push(const T& el);
    bool push(T&& el);

    void front(T& el) const;
    void pop();
    void clear();

protected:
    std::queue<T> m_Queue{};
    mutable std::mutex m_QueueMutex{};
};

template<typename T>
bool QueueThreadSafe<T>::empty() const
{
    bool isEmpty{};

    {
        std::lock_guard lk(m_QueueMutex);
        isEmpty = m_Queue.empty();
    }

    return isEmpty;
}

template<typename T>
size_t QueueThreadSafe<T>::size() const
{
    size_t sz{};

    {
        std::lock_guard lk(m_QueueMutex);
        sz = m_Queue.size();
    }

    return sz;
}

template<typename T>
bool QueueThreadSafe<T>::push(const T& el)
{
    {
        std::lock_guard lk(m_QueueMutex);
        m_Queue.push(el);
    }

    return true;
}

template<typename T>
bool QueueThreadSafe<T>::push(T&& el)
{
    {
        std::lock_guard lk(m_QueueMutex);
        m_Queue.push(std::move(el));
    }

    return true;
}

template<typename T>
void QueueThreadSafe<T>::front(T& el) const
{
    {
        std::lock_guard lk(m_QueueMutex);
        el = m_Queue.front();
    }
}

template<typename T>
void QueueThreadSafe<T>::pop()
{
    {
        std::lock_guard lk(m_QueueMutex);
        m_Queue.pop();
    }
}

template<typename T>
void QueueThreadSafe<T>::clear()
{
    {
        std::lock_guard lk(m_QueueMutex);
        m_Queue.clear();
    }
}

} // namespace i3
