#include "I3/MessageDispatcher.h"

#include "I3/Common/Log.h"
#include "I3/Common/Profile.h"
#include "I3/Common/Guard.h"

#include <vector>
#include <list>
#include <set>
#include <thread>
#include <atomic>
#include <mutex>
#include <iterator>
#include <numeric>

namespace i3
{

struct DataSourceCmp
{
    bool operator() (const std::shared_ptr<DataSource>& lhs, const std::shared_ptr<DataSource>& rhs) const
    {
        i3LogAssert_NonNull(lhs);
        i3LogAssert_NonNull(rhs);

        const uint64_t cmpLhs = static_cast<uint64_t>(lhs->getProtocolID()) | static_cast<uint64_t>(lhs->getSourceID()) << 32;
        const uint64_t cmpRhs = static_cast<uint64_t>(rhs->getProtocolID()) | static_cast<uint64_t>(rhs->getSourceID()) << 32;

        return cmpLhs < cmpRhs;
    }
};

struct ProtocolCmp
{
    bool operator() (const std::shared_ptr<Protocol>& lhs, const std::shared_ptr<Protocol>& rhs) const
    {
        i3LogAssert_NonNull(lhs);
        i3LogAssert_NonNull(rhs);

        ProtocolResult<types::bytespan> magicWordLhs = lhs->magicWord();
        ProtocolResult<types::bytespan> magicWordRhs = rhs->magicWord();

        i3LogAssert(magicWordLhs.isOk());
        i3LogAssert(magicWordRhs.isOk());

        if (magicWordLhs->size() != magicWordRhs->size())
        {
            return magicWordLhs->size() < magicWordRhs->size();
        }

        return (memcmp(magicWordLhs->data(), magicWordRhs->data(), magicWordLhs->size()) < 0);
    }
};

class DataProviderWorker
{
public:
    DataProviderWorker(MessageDispatcher& messageDispatcher, std::shared_ptr<DataProvider> dataProvider);
    ~DataProviderWorker();

    void start();
    void stop();

protected:
    void run();

protected:
    MessageDispatcher& m_MessageDispatcher; // NOTE: A Worker can't overlive its Dispatcher
    std::shared_ptr<DataProvider> m_DataProvider{};

    std::thread m_ThreadWorker{};
    std::atomic_bool m_CancellationToken{};
};

class MessageDispatcher::Impl
{
public:
    explicit MessageDispatcher::Impl(MessageDispatcher& face);

    Status<ErrorCode> addProtocol(std::shared_ptr<Protocol> protocol);
    Status<ErrorCode> addSource(std::shared_ptr<DataSource> dataSource);
    void addProvider(std::shared_ptr<DataProvider> dataProvider);

    Status<ErrorCode> dispatch(types::bytespan message);

    Statistics statistics() const;

protected:
    Result<std::shared_ptr<Protocol>, ErrorCode> recognizeProtocol(types::bytespan message) const;
    Result<std::shared_ptr<DataSource>, ErrorCode> lookupDataSource(crc32_t protocolID, crc32_t sourceID) const;

protected:
    MessageDispatcher& m_Interface; // TODO: fix

    std::set<std::shared_ptr<DataSource>, DataSourceCmp> m_DataSources{};
    mutable std::mutex m_DataSourcesMutex{};

    std::set<std::shared_ptr<Protocol>, ProtocolCmp> m_MessageProtocols{};
    mutable std::mutex m_MessageProtocolsMutex{};

    std::list<DataProviderWorker> m_Workers{};

    std::atomic<int32_t> m_MessagePassedCount{0};
    std::atomic<int32_t> m_MessageDroppedCount{0};
    std::atomic<int32_t> m_MessageRefusedCount{0};
};

DataProviderWorker::DataProviderWorker(MessageDispatcher& messageDispatcher, std::shared_ptr<DataProvider> dataProvider)
    : m_MessageDispatcher{messageDispatcher}
    , m_DataProvider{dataProvider}
{
}

DataProviderWorker::~DataProviderWorker()
{
    stop();
}

void DataProviderWorker::start()
{
    i3LogAssert(!m_ThreadWorker.joinable());

    m_CancellationToken = false;
    m_ThreadWorker = std::thread(&DataProviderWorker::run, this);
}

void DataProviderWorker::stop()
{
    if (m_ThreadWorker.joinable())
    {
        m_CancellationToken = true;
        m_ThreadWorker.join();
    }
}

void DataProviderWorker::run()
{
    i3LogAssert_NonNull(m_DataProvider);

    while (!m_CancellationToken)
    {
        const Result<types::bytespan, DataProvider::ErrorCode> chunk = m_DataProvider->read();
        if (!chunk.isOk())
        {
            if (chunk.code() == DataProvider::ErrorCode::Skip)
            {
                continue;
            }

            auto errToStr = [](DataProvider::ErrorCode code) -> const char*
            {
                switch (code)
                {
                case DataProvider::ErrorCode::EOS: return "End Of Stream";
                case DataProvider::ErrorCode::EmptyChunk: return "Empty Chunk";
                default: return "Unknown";
                }
            };

            i3LogWarn("Couldn't read a new chunk (reason: `%s`)!", errToStr(chunk.code()));

            if (chunk.code() == DataProvider::ErrorCode::EOS)
            {
                break;
            }

            continue;
        }

        //printf("dispatching...\n\n\n");

        auto status = m_MessageDispatcher.dispatch(*chunk);
        if (!status.isOk())
        {
            auto errToStr = [](MessageDispatcher::ErrorCode code) -> const char*
            {
                switch (code)
                {
                case MessageDispatcher::ErrorCode::ProtocolDuplication: return "Protocol Duplication";
                case MessageDispatcher::ErrorCode::ProtocolUnrecognized: return "Protocol Unrecognized";
                case MessageDispatcher::ErrorCode::ProtocolBroken: return "Protocol Broken";
                case MessageDispatcher::ErrorCode::SourceDuplication: return "Source Duplication";
                case MessageDispatcher::ErrorCode::SourceNotFound: return "Source Not Found";
                case MessageDispatcher::ErrorCode::MessageRefused: return "Message Refused";
                default: return "Unknown";
                }
            };

            i3LogWarn("Message dispatch has failed (reason: `%s`)!", errToStr(status.code()));
        }
    }

    i3LogInfo("Shutdown data-provider worker thread successfully");
}

MessageDispatcher::Impl::Impl(MessageDispatcher& face)
    : m_Interface{face}
{
}

Status<MessageDispatcher::ErrorCode> MessageDispatcher::Impl::addProtocol(std::shared_ptr<Protocol> protocol)
{
    auto status = Status<ErrorCode>::ok();

    bool alreadyDefined = false;
    {
        std::lock_guard lk(m_MessageProtocolsMutex);
        const auto [it, inserted] = m_MessageProtocols.insert(protocol);

        alreadyDefined = !inserted;
    }

    if (alreadyDefined)
    {
        ProtocolResult<types::bytespan> protocolName = protocol->magicWord();
        constexpr const char* kNoNameMessage{"Failed to retrieve a name!"};
        types::span<char> name = protocolName.isOk() ?
                                 types::span<char>{reinterpret_cast<const char*>(protocolName->data()), protocolName->size()} :
                                 types::span<char>{kNoNameMessage, sizeof(kNoNameMessage) - 1};

        i3LogWarn("Couldn't insert MessageProtocol=`%.*s`! The following protocol has been already defined!",
                    name.size(),
                    name.data());

        status = Status<ErrorCode>::make(ErrorCode::ProtocolDuplication);
    }

    return status;
}

Status<MessageDispatcher::ErrorCode> MessageDispatcher::Impl::addSource(std::shared_ptr<DataSource> dataSource)
{
    i3LogAssert_NonNull(dataSource);

    auto status = Status<ErrorCode>::ok();

    bool alreadyDefined = false;
    {
        std::lock_guard lk(m_DataSourcesMutex);
        const auto [it, inserted] = m_DataSources.insert(dataSource);

        alreadyDefined = !inserted;
    }

    if (alreadyDefined)
    {
        i3LogWarn("Couldn't insert DataSource=`%0xX`! The following DataSource has been already defined!",
                  dataSource->getSourceID());

        status = Status<ErrorCode>::make(ErrorCode::SourceDuplication);
    }

    return status;
}

void MessageDispatcher::Impl::addProvider(std::shared_ptr<DataProvider> dataProvider)
{
    DataProviderWorker& worker = m_Workers.emplace_back(m_Interface, dataProvider);
    worker.start();
}

Status<MessageDispatcher::ErrorCode> MessageDispatcher::Impl::dispatch(types::bytespan message)
{
    i3ProfileFunction;

    Guard onFail([this]()
    {
        m_MessageDroppedCount++;
    });

    const Result<std::shared_ptr<Protocol>, ErrorCode> protocol = recognizeProtocol(message);
    if (!protocol.isOk())
    {
        i3LogWarn("Couldn't dispatch a message! Failed to recognize a MessageProtocol!");
        return Status<ErrorCode>::make(protocol.code());
    }

    ProtocolResult<types::bytespan> magicWordPicked = (*protocol)->pick(PredefinedHash_MagicWord, message);
    if (!magicWordPicked.isOk())
    {
        i3LogWarn("Couldn't dispatch a message! Failed to pick magic-word!");
        return Status<ErrorCode>::make(ErrorCode::ProtocolBroken);
    }

    ProtocolResult<types::bytespan> sourceIDPicked = (*protocol)->pick(PredefinedHash_SourceID, message);
    if (!sourceIDPicked.isOk())
    {
        i3LogWarn("Couldn't dispatch a message! Failed to pick source-id!");
        return Status<ErrorCode>::make(ErrorCode::ProtocolBroken);
    }

    const crc32_t protocolID = crc32(magicWordPicked->data(), magicWordPicked->size());
    const crc32_t sourceID = crc32(sourceIDPicked->data(), sourceIDPicked->size());

    Result<std::shared_ptr<DataSource>, ErrorCode> dataSource = lookupDataSource(protocolID, sourceID);
    if (!dataSource.isOk())
    {
        i3LogWarn("Couldn't dispatch a message! Failed to lookup a DataSource!");
        return Status<ErrorCode>::make(dataSource.code());
    }

    if (!(*dataSource)->push(message))
    {
        m_MessageRefusedCount++;
        return Status<ErrorCode>::make(ErrorCode::MessageRefused);
    }

    onFail.reset();

    m_MessagePassedCount++;

    return Status<ErrorCode>::ok();
}

MessageDispatcher::Statistics MessageDispatcher::Impl::statistics() const
{
    Statistics stats{};

    stats.MessagePassed = m_MessagePassedCount;
    stats.MessageDropped = m_MessageDroppedCount;
    stats.MessageRefused = m_MessageRefusedCount;

    std::vector<std::shared_ptr<DataSource>> dataSources{};

    // copy in order to not block resource
    {
        std::lock_guard lk(m_DataSourcesMutex);
        std::copy(m_DataSources.begin(), m_DataSources.end(), std::back_inserter(dataSources));
    }

    stats.DataSources.reserve(dataSources.size());
    for (auto source : dataSources)
    {
        auto& ds = stats.DataSources.emplace_back();
        ds.SourceID = source->getSourceID();
        ds.BitrateIn = source->bitrateIn();
        ds.BitrateOut = source->bitrateOut();
        ds.BitrateRead = source->bitrateRead();
    }

    return stats;
}

Result<std::shared_ptr<Protocol>, MessageDispatcher::ErrorCode>
MessageDispatcher::Impl::recognizeProtocol(types::bytespan message) const
{
    std::vector<std::shared_ptr<Protocol>> protocols{};

    // copy in order to not block resource
    {
        std::lock_guard lk(m_MessageProtocolsMutex);
        std::copy(m_MessageProtocols.begin(), m_MessageProtocols.end(), std::back_inserter(protocols));
    }

    const auto it = std::find_if(protocols.begin(),
                                 protocols.end(),
                                 [message](const std::shared_ptr<Protocol>& protocol)
    {
        i3LogAssert_NonNull(protocol);

        const ProtocolResult<bool> matched = protocol->matchMagicWord(message);
        return matched.isOk() && *matched;
    });

    if (it == protocols.end())
    {
        i3LogWarn("Couldn't recognize a protocol (the first bytes are: `%.*s`)!",
                  std::min(message.size(), Protocol::kMagicWordMaxSize),
                  message.data());

        return Result<std::shared_ptr<Protocol>, ErrorCode>::make(ErrorCode::ProtocolUnrecognized);
    }

    return Result<std::shared_ptr<Protocol>, ErrorCode>::make(*it);
}

Result<std::shared_ptr<DataSource>, MessageDispatcher::ErrorCode>
MessageDispatcher::Impl::lookupDataSource(crc32_t protocolID, crc32_t sourceID) const
{
    std::vector<std::shared_ptr<DataSource>> dataSources{};

    // copy in order to not block resource
    {
        std::lock_guard lk(m_DataSourcesMutex);
        std::copy(m_DataSources.begin(), m_DataSources.end(), std::back_inserter(dataSources));
    }

    const auto it = std::find_if(dataSources.begin(),
                                 dataSources.end(),
                                 [protocolID, sourceID](const std::shared_ptr<DataSource>& dataSource)
    {
        i3LogAssert_NonNull(dataSource);

        return dataSource != nullptr &&
               dataSource->getProtocolID() == protocolID &&
               dataSource->getSourceID() == sourceID;
    });

    if (it == dataSources.end())
    {
        i3LogWarn("Couldn't lookup a DataSource (protocol-id=`0x%X`, source-id=`0x%X`)!", protocolID, sourceID);
        return Result<std::shared_ptr<DataSource>, ErrorCode>::make(MessageDispatcher::ErrorCode::SourceNotFound);
    }

    return Result<std::shared_ptr<DataSource>, ErrorCode>::make(*it);
}

uint64_t MessageDispatcher::Statistics::bitrateIn() const
{
    return std::accumulate(DataSources.begin(),
                           DataSources.end(),
                           uint64_t{0},
                           [](uint64_t lhs, const DataSource& ds)
    {
        return lhs + ds.BitrateIn;
    });
}

uint64_t MessageDispatcher::Statistics::bitrateOut() const
{
    return std::accumulate(DataSources.begin(),
                           DataSources.end(),
                           uint64_t{0},
                           [](uint64_t lhs, const DataSource& ds)
    {
        return lhs + ds.BitrateOut;
    });
}

uint64_t MessageDispatcher::Statistics::bitrateRead() const
{
    return std::accumulate(DataSources.begin(),
                           DataSources.end(),
                           uint64_t{0},
                           [](uint64_t lhs, const DataSource& ds)
    {
        return lhs + ds.BitrateRead;
    });
}

MessageDispatcher::MessageDispatcher()
    : m_Impl{std::make_unique<MessageDispatcher::Impl>(*this)}
{
}

MessageDispatcher::MessageDispatcher(MessageDispatcher&& rhs)
    : m_Impl{std::move(rhs.m_Impl)}
{
}

MessageDispatcher& MessageDispatcher::operator=(MessageDispatcher&& rhs)
{
    m_Impl = std::move(rhs.m_Impl);
    return *this;
}

MessageDispatcher::~MessageDispatcher() {}

Status<MessageDispatcher::ErrorCode> MessageDispatcher::dispatch(types::bytespan message)
{
    i3LogAssert_NonNull(m_Impl);

    return m_Impl->dispatch(message);
}

Status<MessageDispatcher::ErrorCode> MessageDispatcher::addProtocol(std::shared_ptr<Protocol> protocol)
{
    i3LogAssert_NonNull(m_Impl);

    return m_Impl->addProtocol(protocol);
}

Status<MessageDispatcher::ErrorCode> MessageDispatcher::addSource(std::shared_ptr<DataSource> dataSource)
{
    i3LogAssert_NonNull(m_Impl);

    return m_Impl->addSource(dataSource);
}

void MessageDispatcher::addProvider(std::shared_ptr<DataProvider> dataProvider)
{
    i3LogAssert_NonNull(m_Impl);

    m_Impl->addProvider(dataProvider);
}

MessageDispatcher::Statistics MessageDispatcher::statistics() const
{
    i3LogAssert_NonNull(m_Impl);

    return m_Impl->statistics();
}

} // namespace i3
