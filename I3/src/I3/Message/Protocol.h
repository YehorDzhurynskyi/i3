#pragma once

#include "I3/Common/Types.h"
#include "I3/Common/Result.h"
#include "I3/Common/CRC32.h"

#include <cstdint>
#include <array>
#include <vector>
#include <cstdarg>
#include <memory>

namespace i3
{

enum PredefinedHash : crc32_t
{
    PredefinedHash_MagicWord = i3CRC32_CompileTime(MagicWord),
    PredefinedHash_SourceID = i3CRC32_CompileTime(SourceID)
};

#define i3MPD_FIELD(s, f)   \
    static_cast<int32_t>(offsetof(s, f)), \
    i3::types::type_of_v<decltype(s::f)>

enum class ProtocolErrorCode
{
    OK,

    NotSupported,
    InvalidProtocol,
    UnmatchedProtocol,
    OutOfBounds,
    NoCountFieldForRepeatedField,
    InvalidSize,
    UnsignedValueExpected,
};

template<typename T>
using ProtocolResult = Result<T, ProtocolErrorCode>;
using ProtocolStatus = Status<ProtocolErrorCode>;

class Protocol
{
public:
    constexpr static size_t kMagicWordMaxSize{6};

    explicit Protocol(std::initializer_list<uint8_t> magicword);
    explicit Protocol(types::bytespan magicword);

    Protocol(Protocol&&);
    Protocol& operator=(Protocol&&);
    ~Protocol(); // For PImpl

    template<typename MsgHeader, crc32_t ... Repeated>
    static ProtocolStatus parse(types::bytespan message, MsgHeader* msg, ...);

    template<typename MsgHeader, crc32_t ... Repeated>
    static ProtocolStatus dump(std::vector<uint8_t>& dest, const MsgHeader* msg, ...);

    crc32_t add(crc32_t fieldHash, int32_t offset, types::Type type, uint32_t count = 1);
    crc32_t addRepeated(crc32_t fieldHash, int32_t offset, types::Type type, crc32_t fieldHashOfCount);

    bool has(crc32_t fieldHash) const;

    ProtocolResult<types::bytespan> pick(crc32_t fieldHash, types::bytespan message) const;

    ProtocolResult<int64_t> unpackSigned(crc32_t fieldHash, types::bytespan message) const;
    ProtocolResult<uint64_t> unpackUnsigned(crc32_t fieldHash, types::bytespan message) const;

    ProtocolResult<types::bytespan> magicWord() const;
    ProtocolResult<bool> matchMagicWord(types::bytespan message) const;

protected:
    ProtocolStatus parse(types::bytespan message,
                         types::bytespan_mutable header,
                         types::span<crc32_t> repeatedFieldHashes,
                         va_list repeated) const;

    ProtocolStatus dump(std::vector<uint8_t>& dest,
                        types::bytespan header,
                        types::span<crc32_t> repeatedFieldHashes,
                        va_list repeated) const;

protected:
    class Impl;
    std::unique_ptr<Impl> m_Impl{};
};

template<typename MsgHeader, crc32_t ... Repeated>
static ProtocolStatus Protocol::parse(types::bytespan message, MsgHeader* msg, ...)
{
    static_assert(std::alignment_of_v<MsgHeader> == 1);

    constexpr size_t kRepeatedCount = sizeof ... (Repeated);
    constexpr std::array<crc32_t, kRepeatedCount> kRepeatedFieldHashes{Repeated...};

    const Protocol protocol = MsgHeader::makeProtocol();

    va_list repeated{};
    va_start(repeated, msg);

    types::bytespan_mutable header{reinterpret_cast<uint8_t*>(msg), sizeof(MsgHeader)};
    const types::span<crc32_t> hashes{kRepeatedFieldHashes.data(), kRepeatedFieldHashes.size()};

    const ProtocolStatus status = protocol.parse(message, header, hashes, repeated);

    va_end(repeated);

    return status;
}

template<typename MsgHeader, crc32_t ... Repeated>
ProtocolStatus Protocol::dump(std::vector<uint8_t>& dest, const MsgHeader* msg, ...)
{
    static_assert(std::alignment_of_v<MsgHeader> == 1);

    i3LogAssert_NonNull(msg);

    constexpr size_t kRepeatedCount = sizeof ... (Repeated);
    constexpr std::array<crc32_t, kRepeatedCount> kRepeatedFieldHashes{Repeated...};

    const Protocol protocol = MsgHeader::makeProtocol();

    va_list repeated{};
    va_start(repeated, msg);

    const types::bytespan header{reinterpret_cast<const uint8_t*>(msg), sizeof(MsgHeader)};
    const types::span<crc32_t> hashes{kRepeatedFieldHashes.data(), kRepeatedFieldHashes.size()};

    const ProtocolStatus status = protocol.dump(dest, header, hashes, repeated);

    va_end(repeated);

    return status;
}

} // namespace i3
