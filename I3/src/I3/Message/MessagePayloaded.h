#pragma once

#include "I3/Message/Protocol.h"

#include <vector>
#include <array>

namespace i3
{

// NOTE: this class isn't thread-safe
// you can't parse/dump the message and
// changing the header/payload in the same time
template<typename MsgHeader, crc32_t PayloadHash>
struct MessagePayloaded
{
    static_assert(std::alignment_of_v<MsgHeader> == 1);

    using header_type = MsgHeader;

    ProtocolStatus parse(types::bytespan message);
    ProtocolStatus dump(std::vector<uint8_t>& dest) const;

    bool operator==(const MessagePayloaded<MsgHeader, PayloadHash>& rhs) const;
    bool operator!=(const MessagePayloaded<MsgHeader, PayloadHash>& rhs) const;

    MsgHeader Header{};
    std::vector<uint8_t> Payload{};
};

template<typename MsgHeader, crc32_t PayloadHash>
ProtocolStatus MessagePayloaded<MsgHeader, PayloadHash>::parse(types::bytespan message)
{
    return Protocol::parse<MsgHeader, PayloadHash>(message, &Header, &Payload);
}

template<typename MsgHeader, crc32_t PayloadHash>
ProtocolStatus MessagePayloaded<MsgHeader, PayloadHash>::dump(std::vector<uint8_t>& dest) const
{
    const types::bytespan payload{Payload.data(), Payload.size()};
    return Protocol::dump<MsgHeader, PayloadHash>(dest, &Header, payload);
}

template<typename MsgHeader, crc32_t PayloadHash>
bool MessagePayloaded<MsgHeader, PayloadHash>::operator==(const MessagePayloaded<MsgHeader, PayloadHash>& rhs) const
{
    static_assert(std::alignment_of_v<MsgHeader> == 1);

    const bool headersAreEqual = (memcmp(&Header, &rhs.Header, sizeof(MsgHeader)) == 0);
    if (!headersAreEqual)
    {
        return false;
    }

    return Payload == rhs.Payload;
}

template<typename MsgHeader, crc32_t PayloadHash>
bool MessagePayloaded<MsgHeader, PayloadHash>::operator!=(const MessagePayloaded <MsgHeader, PayloadHash>& rhs) const
{
    return !(*this == rhs);
}

} // namespace i3
