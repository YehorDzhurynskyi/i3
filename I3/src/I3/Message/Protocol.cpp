#pragma once

#include "I3/Message/Protocol.h"

#include "I3/Common/Log.h"

#include <optional>
#include <algorithm>
#include <map>

namespace i3
{

struct Field
{
    Field(const Field&) = delete;
    Field& operator=(const Field&) = delete;
    Field(Field&&) = default;
    Field& operator=(Field&&) = default;
    ~Field() = default;

    Field(int32_t offset, types::Type datatype, uint32_t count = 1);

    ProtocolResult<size_t> size() const;

    types::Type elementType() const;

    int32_t begin() const;
    ProtocolResult<int32_t> end() const;

    ProtocolResult<types::bytespan> pick(types::bytespan message) const;

protected:
    int32_t m_Offset{};
    types::Type m_ElementType{};
    uint32_t m_Count{1};
};

struct FieldRepeated
{
    FieldRepeated(const FieldRepeated&) = delete;
    FieldRepeated& operator=(const FieldRepeated&) = delete;
    FieldRepeated(FieldRepeated&&) = default;
    FieldRepeated& operator=(FieldRepeated&&) = default;
    ~FieldRepeated() = default;

    FieldRepeated(int32_t offset, types::Type datatype, const Field& fieldHashOfCount);

    ProtocolResult<size_t> size(types::bytespan message) const;

    int32_t begin() const;
    ProtocolResult<int32_t> end(types::bytespan message) const;

    ProtocolResult<types::bytespan> pick(types::bytespan message) const;

protected:
    int32_t m_Offset{};
    types::Type m_DataType{};
    const Field& m_FieldHashOfCount;
};

class Protocol::Impl
{
public:
    explicit Protocol::Impl(types::bytespan magicword);

    crc32_t add(crc32_t fieldHash, int32_t offset, types::Type type, uint32_t count = 1);
    crc32_t addRepeated(crc32_t fieldHash, int32_t offset, types::Type type, crc32_t fieldHashOfCount);

    bool has(crc32_t fieldHash) const;

    const Field* find(crc32_t fieldHash) const;
    const FieldRepeated* findRepeated(crc32_t fieldHash) const;

    ProtocolResult<types::bytespan> pick(crc32_t fieldHash, types::bytespan message) const;

    ProtocolResult<int64_t> unpackSigned(crc32_t fieldHash, types::bytespan message) const;
    ProtocolResult<uint64_t> unpackUnsigned(crc32_t fieldHash, types::bytespan message) const;

    ProtocolResult<types::bytespan> magicWord() const;
    ProtocolResult<bool> matchMagicWord(types::bytespan message) const;

    ProtocolStatus parse(types::bytespan message,
                         types::bytespan_mutable header,
                         types::span<crc32_t> repeatedFieldHashes,
                         va_list repeated) const;

    ProtocolStatus dump(std::vector<uint8_t>& dest,
                        types::bytespan header,
                        types::span<crc32_t> repeatedFieldHashes,
                        va_list repeated) const;

protected:
    using MagicWord_t = std::array<uint8_t, kMagicWordMaxSize>;

    MagicWord_t m_MagicWord{};

    std::map<crc32_t, Field> m_Fields{};
    std::map<crc32_t, FieldRepeated> m_FieldsRepeated{};
};

Protocol::Impl::Impl(types::bytespan magicword)
{
    i3LogAssert(magicword.size() > 0 && magicword.size() <= kMagicWordMaxSize);

    std::copy_n(magicword.begin(), magicword.size(), m_MagicWord.begin());

    add(PredefinedHash_MagicWord, 0, types::UInt8, static_cast<uint32_t>(magicword.size()));
}

crc32_t Protocol::Impl::add(crc32_t fieldHash, int32_t offset, types::Type type, uint32_t count)
{
    auto [it, inserted] = m_Fields.try_emplace(fieldHash, offset, type, count);

    // TODO: handle error
    i3LogAssert(inserted);

    return fieldHash;
}

crc32_t Protocol::Impl::addRepeated(crc32_t fieldHash, int32_t offset, types::Type type, crc32_t fieldHashOfCount)
{
    // NOTE: at this moment Protocol supports only a single repeated field
    i3LogAssert(m_FieldsRepeated.empty());

    const Field* fieldCount = find(fieldHashOfCount);

    // TODO: handle error
    i3LogAssert_NonNull(fieldCount);

    auto [it, inserted] = m_FieldsRepeated.try_emplace(fieldHash, offset, type, *fieldCount);

    // TODO: handle error
    i3LogAssert(inserted);

    return fieldHash;
}

bool Protocol::Impl::has(crc32_t fieldHash) const
{
    return find(fieldHash) != nullptr || findRepeated(fieldHash);
}

const Field* Protocol::Impl::find(crc32_t fieldHash) const
{
    const auto it = m_Fields.find(fieldHash);

    if (it == m_Fields.end())
    {
        return nullptr;
    }

    return &it->second;
}

const FieldRepeated* Protocol::Impl::findRepeated(crc32_t fieldHash) const
{
    const auto it = m_FieldsRepeated.find(fieldHash);

    if (it == m_FieldsRepeated.end())
    {
        return nullptr;
    }

    return &it->second;
}

ProtocolResult<types::bytespan> Protocol::Impl::pick(crc32_t fieldHash, types::bytespan message) const
{
    if (!has(fieldHash))
    {
        return ProtocolResult<types::bytespan>::make(ProtocolErrorCode::NotSupported);
    }

    if (const Field* field = find(fieldHash))
    {
        return field->pick(message);
    }
    else if (const FieldRepeated* fieldRepeated = findRepeated(fieldHash))
    {
        return fieldRepeated->pick(message);
    }

    return ProtocolResult<types::bytespan>::make(ProtocolErrorCode::NotSupported);
}

ProtocolResult<int64_t> Protocol::Impl::unpackSigned(crc32_t fieldHash, types::bytespan message) const
{
    const Field* field = find(fieldHash);
    if (!field)
    {
        return ProtocolResult<int64_t>::make(ProtocolErrorCode::NotSupported);
    }

    ProtocolResult<types::bytespan> span = field->pick(message);
    if (!span.isOk())
    {
        return ProtocolResult<int64_t>::make(span.code());
    }

    return ProtocolResult<int64_t>::make(types::UnpackSigned(span->data(), field->elementType()));
}

ProtocolResult<uint64_t> Protocol::Impl::unpackUnsigned(crc32_t fieldHash, types::bytespan message) const
{
    const Field* field = find(fieldHash);
    if (!field)
    {
        return ProtocolResult<uint64_t>::make(ProtocolErrorCode::NotSupported);
    }

    ProtocolResult<types::bytespan> span = field->pick(message);
    if (!span.isOk())
    {
        return ProtocolResult<uint64_t>::make(span.code());
    }

    std::optional<uint64_t> unsign = types::UnpackUnsigned(span->data(), field->elementType());
    if (!unsign)
    {
        return ProtocolResult<uint64_t>::make(ProtocolErrorCode::UnsignedValueExpected);
    }

    return ProtocolResult<uint64_t>::make(*unsign);
}

ProtocolResult<types::bytespan> Protocol::Impl::magicWord() const
{
    const Field* magicWordField = find(PredefinedHash_MagicWord);
    if (!magicWordField)
    {
        return ProtocolResult<types::bytespan>::make(ProtocolErrorCode::InvalidProtocol);
    }

    const ProtocolResult<size_t> magicWordSize = magicWordField->size();
    if (!magicWordSize.isOk())
    {
        return ProtocolResult<types::bytespan>::make(magicWordSize.code());
    }

    return ProtocolResult<types::bytespan>::make(types::bytespan{m_MagicWord.data(), *magicWordSize});
}

ProtocolResult<bool> Protocol::Impl::matchMagicWord(types::bytespan message) const
{
    ProtocolResult<types::bytespan> magicWordExpected = magicWord();
    if (!magicWordExpected.isOk())
    {
        return ProtocolResult<bool>::make(magicWordExpected.code());
    }

    ProtocolResult<types::bytespan> magicWord = pick(PredefinedHash_MagicWord, message);
    if (!magicWord.isOk())
    {
        return ProtocolResult<bool>::make(magicWord.code());
    }

    if (message.size() < magicWordExpected->size())
    {
        return ProtocolResult<bool>::make(false);
    }

    const bool matched = (memcmp(message.data(), magicWordExpected->data(), magicWordExpected->size()) == 0);

    return ProtocolResult<bool>::make(matched);
}

ProtocolStatus Protocol::Impl::parse(types::bytespan message,
                                     types::bytespan_mutable header,
                                     types::span<crc32_t> repeatedFieldHashes,
                                     va_list repeated) const
{
    ProtocolResult<bool> matched = matchMagicWord(message);
    if (!matched.isOk())
    {
        return ProtocolStatus::make(matched.code());
    }

    if (!*matched)
    {
        return ProtocolStatus::make(ProtocolErrorCode::UnmatchedProtocol);
    }

    const int32_t sz = static_cast<int32_t>(header.size());
    for (const auto& [fieldHash, field] : m_Fields)
    {
        const ProtocolResult<types::bytespan> fieldraw = field.pick(message);
        if (!fieldraw.isOk())
        {
            return ProtocolStatus::make(fieldraw.code());
        }

        memcpy_s(header.data() + field.begin(), sz - field.begin(), fieldraw->data(), fieldraw->size());
    }

    for (const crc32_t repeatedFieldHash : repeatedFieldHashes)
    {
        const FieldRepeated* field = findRepeated(repeatedFieldHash);
        if (!field)
        {
            return ProtocolStatus::make(ProtocolErrorCode::NotSupported);
        }

        ProtocolResult<types::bytespan> fieldraw = field->pick(message);
        if (!fieldraw.isOk())
        {
            return ProtocolStatus::make(fieldraw.code());
        }

        std::vector<uint8_t>* payload = va_arg(repeated, std::vector<uint8_t>*);
        i3LogAssert_NonNull(payload);

        payload->resize(fieldraw->size());

        memcpy_s(payload->data(), payload->size(), fieldraw->data(), fieldraw->size());
    }

    return ProtocolStatus::ok();
}

ProtocolStatus Protocol::Impl::dump(std::vector<uint8_t>& dest,
                                    types::bytespan header,
                                    types::span<crc32_t> repeatedFieldHashes,
                                    va_list repeated) const
{
    int32_t sz{};
    for (const auto& [fieldHash, field] : m_Fields)
    {
        const ProtocolResult<size_t> fieldSize = field.size();
        if (!fieldSize.isOk())
        {
            return ProtocolStatus::make(fieldSize.code());
        }

        sz += static_cast<int32_t>(*fieldSize);
    }

    dest.resize(sz);

    for (const auto& [fieldHash, field] : m_Fields)
    {
        memcpy_s(dest.data() + field.begin(), sz - field.begin(), header.data() + field.begin(), *field.size());
    }

    for (const crc32_t repeatedFieldHash : repeatedFieldHashes)
    {
        const FieldRepeated* field = findRepeated(repeatedFieldHash);
        if (!field)
        {
            return ProtocolStatus::make(ProtocolErrorCode::NotSupported);
        }

        ProtocolResult<size_t> fsz = field->size(header);
        if (!fsz.isOk())
        {
            return ProtocolStatus::make(fsz.code());
        }

        sz += static_cast<int32_t>(*fsz);
    }

    dest.resize(sz);

    for (const crc32_t repeatedFieldHash : repeatedFieldHashes)
    {
        const FieldRepeated* field = findRepeated(repeatedFieldHash);
        if (!field)
        {
            return ProtocolStatus::make(ProtocolErrorCode::NotSupported);
        }

        ProtocolResult<size_t> fsz = field->size(types::bytespan{dest.data(), dest.size()});
        if (!fsz.isOk())
        {
            return ProtocolStatus::make(fsz.code());
        }

        const types::bytespan payload = va_arg(repeated, types::bytespan);
        if (payload.size() < *fsz)
        {
            return ProtocolStatus::make(ProtocolErrorCode::OutOfBounds);
        }

        memcpy_s(dest.data() + field->begin(), sz - field->begin(), payload.data(), *fsz);
    }

    return ProtocolStatus::ok();
}

Field::Field(int32_t offset, types::Type datatype, uint32_t count)
    : m_Offset{offset}
    , m_ElementType{datatype}
    , m_Count{count}
{
}

ProtocolResult<size_t> Field::size() const
{
    const size_t sizeOfElement = SizeOf(m_ElementType);

    if (m_Count <= 0 || sizeOfElement == 0)
    {
        return ProtocolResult<size_t>::make(ProtocolErrorCode::InvalidSize);
    }

    return ProtocolResult<size_t>::make(m_Count * sizeOfElement);
}

types::Type Field::elementType() const
{
    return m_ElementType;
}

int32_t Field::begin() const
{
    return m_Offset;
}

ProtocolResult<int32_t> Field::end() const
{
    ProtocolResult<size_t> sz = size();
    if (!sz.isOk())
    {
        return ProtocolResult<int32_t>::make(sz.code());
    }

    return ProtocolResult<int32_t>::make(m_Offset + static_cast<int32_t>(*sz));
}

ProtocolResult<types::bytespan> Field::pick(types::bytespan message) const
{
    ProtocolResult<size_t> sz = size();
    if (!sz.isOk())
    {
        return ProtocolResult<types::bytespan>::make(sz.code());
    }

    if (message.size() < m_Offset + *sz)
    {
        return ProtocolResult<types::bytespan>::make(ProtocolErrorCode::OutOfBounds);
    }

    return ProtocolResult<types::bytespan>::make(types::bytespan{message.data() + begin(), *sz});
}

FieldRepeated::FieldRepeated(int32_t offset,
                                                           types::Type datatype,
                                                           const Field& fieldHashOfCount)
    : m_Offset{offset}
    , m_DataType{datatype}
    , m_FieldHashOfCount{fieldHashOfCount}
{
}

ProtocolResult<size_t> FieldRepeated::size(types::bytespan message) const
{
    ProtocolResult<types::bytespan> payloadSize = m_FieldHashOfCount.pick(message);
    if (!payloadSize.isOk())
    {
        return ProtocolResult<size_t>::make(payloadSize.code());
    }

    std::optional<uint64_t> sz = types::UnpackUnsigned(payloadSize->data(), m_FieldHashOfCount.elementType());
    if (!sz)
    {
        return ProtocolResult<size_t>::make(ProtocolErrorCode::UnsignedValueExpected);
    }

    return ProtocolResult<size_t>::make(*sz);
}

int32_t FieldRepeated::begin() const
{
    return m_Offset;
}

ProtocolResult<int32_t> FieldRepeated::end(types::bytespan message) const
{
    ProtocolResult<size_t> sz = size(message);
    if (!sz.isOk())
    {
        return ProtocolResult<int32_t>::make(sz.code());
    }

    return ProtocolResult<int32_t>::make(m_Offset + static_cast<int32_t>(*sz));
}

ProtocolResult<types::bytespan> FieldRepeated::pick(types::bytespan message) const
{
    ProtocolResult<size_t> sz = size(message);
    if (!sz.isOk())
    {
        return ProtocolResult<types::bytespan>::make(sz.code());
    }

    if (message.size() < m_Offset + *sz)
    {
        return ProtocolResult<types::bytespan>::make(ProtocolErrorCode::OutOfBounds);
    }

    return ProtocolResult<types::bytespan>::make(types::bytespan{message.data() + begin(), *sz});
}

Protocol::Protocol(std::initializer_list<uint8_t> magicword)
    : Protocol{types::bytespan{magicword.begin(), magicword.size()}}
{
}

Protocol::Protocol(types::bytespan magicword)
    : m_Impl{std::make_unique<Protocol::Impl>(magicword)}
{
}

Protocol::Protocol(Protocol&& rhs)
    : m_Impl{std::move(rhs.m_Impl)}
{
}

Protocol& Protocol::operator=(Protocol&& rhs)
{
    m_Impl = std::move(rhs.m_Impl);

    return *this;
}

Protocol::~Protocol() {}

crc32_t Protocol::add(crc32_t fieldHash, int32_t offset, types::Type type, uint32_t count)
{
    i3LogAssert_NonNull(m_Impl);

    return m_Impl->add(fieldHash, offset, type, count);
}

crc32_t Protocol::addRepeated(crc32_t fieldHash, int32_t offset, types::Type type, crc32_t fieldHashOfCount)
{
    i3LogAssert_NonNull(m_Impl);

    return m_Impl->addRepeated(fieldHash, offset, type, fieldHashOfCount);
}

bool Protocol::has(crc32_t fieldHash) const
{
    i3LogAssert_NonNull(m_Impl);

    return m_Impl->has(fieldHash);
}

ProtocolResult<types::bytespan> Protocol::pick(crc32_t fieldHash, types::bytespan message) const
{
    i3LogAssert_NonNull(m_Impl);

    return m_Impl->pick(fieldHash, message);
}

ProtocolResult<int64_t> Protocol::unpackSigned(crc32_t fieldHash, types::bytespan message) const
{
    i3LogAssert_NonNull(m_Impl);

    return m_Impl->unpackSigned(fieldHash, message);
}

ProtocolResult<uint64_t> Protocol::unpackUnsigned(crc32_t fieldHash, types::bytespan message) const
{
    i3LogAssert_NonNull(m_Impl);

    return m_Impl->unpackUnsigned(fieldHash, message);
}

ProtocolResult<types::bytespan> Protocol::magicWord() const
{
    i3LogAssert_NonNull(m_Impl);

    return m_Impl->magicWord();
}

ProtocolResult<bool> Protocol::matchMagicWord(types::bytespan message) const
{
    i3LogAssert_NonNull(m_Impl);

    return m_Impl->matchMagicWord(message);
}

ProtocolStatus Protocol::parse(types::bytespan message,
                               types::bytespan_mutable header,
                               types::span<crc32_t> repeatedFieldHashes,
                               va_list repeated) const
{
    i3LogAssert_NonNull(m_Impl);

    return m_Impl->parse(message, header, repeatedFieldHashes, repeated);
}

ProtocolStatus Protocol::dump(std::vector<uint8_t>& dest,
                              types::bytespan header,
                              types::span<crc32_t> repeatedFieldHashes,
                              va_list repeated) const
{
    i3LogAssert_NonNull(m_Impl);

    return m_Impl->dump(dest, header, repeatedFieldHashes, repeated);
}

} // namespace i3
