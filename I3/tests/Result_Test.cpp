#include <gtest/gtest.h>

#include "I3/Common/Result.h"

namespace
{

enum class Error
{
    Code200 = 0,

    Code403,
    Code404,

    Code500,
};

} // namespace

TEST(Result, ResultHappyPass)
{
    auto result = i3::Result<int32_t, Error>::make(42);

    EXPECT_TRUE(result.isOk());
    EXPECT_EQ(result.code(), Error::Code200);
    EXPECT_EQ(*result, 42);
}

TEST(Result, ResultFailPass)
{
    auto result = i3::Result<int32_t, Error>::make(Error::Code404);

    EXPECT_FALSE(result.isOk());
    EXPECT_EQ(result.code(), Error::Code404);
}

TEST(Result, StatusHappyPass)
{
    auto result = i3::Status<Error>::ok();

    EXPECT_TRUE(result.isOk());
    EXPECT_EQ(result.code(), Error::Code200);
}

TEST(Result, StatusFailPass)
{
    auto result = i3::Status<Error>::make(Error::Code500);

    EXPECT_FALSE(result.isOk());
    EXPECT_EQ(result.code(), Error::Code500);
}
