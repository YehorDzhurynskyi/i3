#include <gtest/gtest.h>

#include "I3/Common/Clock.h"

#include <limits>

TEST(ClockManualSteady, Smoke)
{
    using namespace std::chrono_literals;

    i3::ClockManualSteady::reset();

    i3::ClockManualSteady::time_point now{};

    now = i3::ClockManualSteady::now();

    EXPECT_EQ(now, i3::ClockManualSteady::time_point{});
    EXPECT_EQ(now, i3::ClockManualSteady::time_point{});
    EXPECT_EQ(now.time_since_epoch(), 0ms);

    // advance
    i3::ClockManualSteady::advance(10ms);

    now = i3::ClockManualSteady::now();

    EXPECT_NE(now, i3::ClockManualSteady::time_point{});
    EXPECT_EQ(now, i3::ClockManualSteady::time_point{} + 10ms);
    EXPECT_EQ(now.time_since_epoch(), 10ms);

    // advance multiple times
    for (int32_t i = 0; i < 4; ++i)
    {
        i3::ClockManualSteady::advance(10ms);
    }

    now = i3::ClockManualSteady::now();

    EXPECT_NE(now, i3::ClockManualSteady::time_point{});
    EXPECT_EQ(now, i3::ClockManualSteady::time_point{} + 50ms);
    EXPECT_EQ(now.time_since_epoch(), 50ms);

    // reset
    i3::ClockManualSteady::reset();

    now = i3::ClockManualSteady::now();

    EXPECT_EQ(now, i3::ClockManualSteady::time_point{});
    EXPECT_EQ(now.time_since_epoch(), 0ms);
}
