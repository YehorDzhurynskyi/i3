#include <gtest/gtest.h>

#include "I3/Common/Clock.h"
#include "I3/Metric/MetricGeneratorCBR.h"

TEST(MetricGeneratorCBR, Smoke)
{
    using namespace std::chrono_literals;

    constexpr int32_t kBitRate{42};

    i3::MetricGeneratorCBR cbr{kBitRate};

    i3::Clock::reset();

    EXPECT_EQ(cbr.bitrate(), kBitRate);

    i3::Clock::advance(100ms);

    EXPECT_EQ(cbr.bitrate(), kBitRate);

    i3::Clock::advance(33ms);

    EXPECT_EQ(cbr.bitrate(), kBitRate);

    i3::Clock::reset();

    EXPECT_EQ(cbr.bitrate(), kBitRate);
}
