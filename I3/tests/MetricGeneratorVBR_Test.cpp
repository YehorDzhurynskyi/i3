#include <gtest/gtest.h>

#include "I3/Common/Clock.h"
#include "I3/Metric/MetricGeneratorVBR.h"

#include <array>

TEST(MetricGeneratorVBR, Smoke)
{
    using namespace std::chrono_literals;

    constexpr int32_t kBitRateMin{10};
    constexpr int32_t kBitRateMax{100};
    constexpr std::array<int32_t,5> kPeriodMSecsArr{
        1000,
        300,
        10,
        333,
        21
    };

    for (int32_t periodMSecs : kPeriodMSecsArr)
    {
        i3::ClockManualSteady::reset();

        i3::MetricGeneratorVBR vbr{kBitRateMin, kBitRateMax, periodMSecs};

        EXPECT_EQ(vbr.bitrate(), kBitRateMax);

        i3::Clock::advance(i3::Clock::duration{periodMSecs});

        EXPECT_EQ(vbr.bitrate(), kBitRateMax);

        i3::Clock::advance(i3::Clock::duration{periodMSecs / 2});

        EXPECT_EQ(vbr.bitrate(), kBitRateMin);
    }
}
