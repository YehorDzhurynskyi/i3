#include <gtest/gtest.h>

#include "I3/Common/Types.h"

TEST(Types, bytespan)
{
    const uint8_t arr[4]{'0', '1', '2', '3'};

    const i3::types::bytespan span(arr, sizeof(arr));

    int32_t i = 0;
    for (uint8_t b : span)
    {
        EXPECT_EQ(b, arr[i++]);
    }

    EXPECT_EQ(i, sizeof(arr));
}
