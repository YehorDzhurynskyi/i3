#include <gtest/gtest.h>

#include "I3/Message/Protocol.h"
#include "I3/Message/MessagePayloaded.h"

#include <array>
#include <optional>

namespace
{

constexpr i3::crc32_t kHashSequenceNumber = i3CRC32_CompileTime(SequenceNumber);
constexpr i3::crc32_t kHashPayloadSize = i3CRC32_CompileTime(PayloadSize);
constexpr i3::crc32_t kHashPayload = i3CRC32_CompileTime(Payload);

constexpr std::array<uint8_t, 5> kMagicWord{'0', 'H', 'T', 'T', 'P'};

#pragma pack(push, 1)
struct MessageHeader
{
    static i3::Protocol makeProtocol();

    std::array<uint8_t, 5> MagicWord{kMagicWord};
    int64_t SequenceNumber{};
    uint32_t SourceID{};
    int16_t PayloadSize{};
};
#pragma pack(pop)

static_assert(sizeof(MessageHeader) == 5 + 8 + 4 + 2);
static_assert(std::alignment_of_v<MessageHeader> == 1);

using Message = i3::MessagePayloaded<MessageHeader, kHashPayload>;

i3::Protocol MessageHeader::makeProtocol()
{
    i3::Protocol protocol({kMagicWord.data(), kMagicWord.size()});

    protocol.add(i3::PredefinedHash_SourceID, i3MPD_FIELD(MessageHeader, SourceID));
    protocol.add(kHashSequenceNumber, i3MPD_FIELD(MessageHeader, SequenceNumber));

    protocol.addRepeated(kHashPayload,
        static_cast<int32_t>(sizeof(MessageHeader)),
        i3::types::Type::UInt8,
        protocol.add(kHashPayloadSize, i3MPD_FIELD(MessageHeader, PayloadSize))
    );

    return protocol;
}

} // namespace

TEST(Protocol, Smoke)
{
    constexpr decltype(MessageHeader::SourceID) kSourceID = 42;
    constexpr decltype(MessageHeader::SequenceNumber) kSequenceNumber = 4308;

    const std::vector<uint8_t> kPayload = {'a', 'b', 'c', 'd', 'e'};

    std::vector<uint8_t> raw{};
    {
        Message m{};
        m.Header.SourceID = kSourceID;
        m.Header.SequenceNumber = kSequenceNumber;
        m.Header.PayloadSize = static_cast<decltype(MessageHeader::PayloadSize)>(kPayload.size());
        std::copy(kPayload.begin(), kPayload.end(), std::back_inserter(m.Payload));

        const i3::ProtocolStatus status = m.dump(raw);
        EXPECT_TRUE(status.isOk());

        {
            Message mCopy{};

            const i3::ProtocolStatus statusParse = mCopy.parse(i3::types::bytespan(raw.data(), raw.size()));
            EXPECT_TRUE(statusParse.isOk());

            EXPECT_EQ(m, mCopy);
        }
    }

    i3::types::bytespan message{raw.data(), raw.size()};
    EXPECT_EQ(message.size(), sizeof(MessageHeader) + kPayload.size());

    const auto protocol = MessageHeader::makeProtocol();

    EXPECT_TRUE(*protocol.matchMagicWord(message));

    const i3::types::bytespan magicWordExpected{kMagicWord.data(), kMagicWord.size()};
    EXPECT_EQ(*protocol.magicWord(), magicWordExpected);

    const int64_t actualSequenceNumber = *protocol.unpackSigned(kHashSequenceNumber, message);
    EXPECT_EQ(actualSequenceNumber, kSequenceNumber);

    const int64_t actualSourceID = *protocol.unpackSigned(i3::PredefinedHash_SourceID, message);
    EXPECT_EQ(actualSourceID, kSourceID);

    const uint64_t actualPayloadSize = *protocol.unpackUnsigned(kHashPayloadSize, message);
    EXPECT_EQ(actualPayloadSize, kPayload.size());

    i3::types::bytespan actualPayload = *protocol.pick(kHashPayload, message);

    const std::vector<uint8_t> actual{actualPayload.begin(), actualPayload.end()};

    EXPECT_EQ(actualPayload.size(), kPayload.size());
    EXPECT_EQ(actual, kPayload);
}

namespace
{

#pragma pack(push, 1)
template<typename T>
struct MessageSizeOnlyHeader
{
    static_assert(std::is_integral_v<T>);

    static i3::Protocol makeProtocol();

    std::array<uint8_t, 4> MagicWord{'S', 'I', 'Z', 'E'};
    T PayloadSize{};
};
#pragma pack(pop)

template<typename T>
using MessageSizeOnly = i3::MessagePayloaded<MessageSizeOnlyHeader<T>, kHashPayload>;

template<typename T>
i3::Protocol MessageSizeOnlyHeader<T>::makeProtocol()
{
    i3::Protocol protocol({'S', 'I', 'Z', 'E'});

    protocol.addRepeated(kHashPayload,
        static_cast<int32_t>(sizeof(MessageSizeOnlyHeader<T>)),
        i3::types::Type::UInt8,
        protocol.add(kHashPayloadSize, i3MPD_FIELD(MessageSizeOnlyHeader<T>, PayloadSize))
    );

    return protocol;
}

template<typename T>
void TestPayloadSize()
{
    static_assert(std::is_integral_v<T>);

    auto testcase = [](T provided, std::optional<size_t> expected)
    {
        std::vector<uint8_t> raw{};
        {
            MessageSizeOnly<T> m{};
            m.Header.PayloadSize = provided;
            m.Payload.resize(std::max<T>(provided, 0));

            const i3::ProtocolStatus status = m.dump(raw);
            if (expected == std::nullopt)
            {
                EXPECT_FALSE(status.isOk());
                return;
            }

            EXPECT_TRUE(status.isOk());

            {
                MessageSizeOnly<T> mCopy{};

                const i3::ProtocolStatus statusParse = mCopy.parse(i3::types::bytespan(raw.data(), raw.size()));
                EXPECT_TRUE(statusParse.isOk());

                EXPECT_EQ(m, mCopy);
            }
        }

        const i3::Protocol protocol = MessageSizeOnlyHeader<T>::makeProtocol();

        const i3::types::bytespan message(raw.data(), raw.size());
        const i3::Result<uint64_t, i3::ProtocolErrorCode> actualPayloadSize = protocol.unpackUnsigned(kHashPayloadSize, message);

        if (expected != std::nullopt)
        {
            EXPECT_TRUE(actualPayloadSize.isOk());
            EXPECT_EQ(*actualPayloadSize, *expected);
        }
        else
        {
            EXPECT_FALSE(actualPayloadSize.isOk());
        }
    };

    testcase(0, 0);
    testcase(42, 42);

    if constexpr (std::is_signed_v<T>)
    {
        testcase(-42, std::nullopt);
    }
}

} // namespace

TEST(Protocol, PayloadSize)
{
    TestPayloadSize<int8_t>();
    TestPayloadSize<uint8_t>();
    TestPayloadSize<int16_t>();
    TestPayloadSize<uint16_t>();
    TestPayloadSize<int32_t>();
    TestPayloadSize<uint32_t>();
    TestPayloadSize<int64_t>();
    TestPayloadSize<uint64_t>();
}
