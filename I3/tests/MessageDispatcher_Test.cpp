#include <gtest/gtest.h>

#include "I3/MessageDispatcher.h"
#include "I3/Message/MessagePayloaded.h"
#include "I3/DataSource/DataSourcePassThrough.h"

namespace
{

constexpr i3::crc32_t kHashSequenceNumber = i3CRC32_CompileTime(SequenceNumber);
constexpr i3::crc32_t kHashPayloadSize = i3CRC32_CompileTime(PayloadSize);
constexpr i3::crc32_t kHashPayload = i3CRC32_CompileTime(Payload);

constexpr std::array<uint8_t, 5> kMagicWord{'0', 'H', 'T', 'T', 'P'};

#pragma pack(push, 1)
struct MessageHeader
{
    static i3::Protocol makeProtocol();

    std::array<uint8_t, 5> MagicWord{kMagicWord};
    int64_t SequenceNumber{};
    uint32_t SourceID{};
    int16_t PayloadSize{};
};
#pragma pack(pop)

static_assert(sizeof(MessageHeader) == 5 + 8 + 4 + 2);
static_assert(std::alignment_of_v<MessageHeader> == 1);

using Message = i3::MessagePayloaded<MessageHeader, kHashPayload>;

i3::Protocol MessageHeader::makeProtocol()
{
    i3::Protocol protocol({kMagicWord.data(), kMagicWord.size()});

    protocol.add(i3::PredefinedHash_SourceID, i3MPD_FIELD(MessageHeader, SourceID));
    protocol.add(kHashSequenceNumber, i3MPD_FIELD(MessageHeader, SequenceNumber));

    protocol.addRepeated(kHashPayload,
                         static_cast<int32_t>(sizeof(MessageHeader)),
                         i3::types::Type::UInt8,
                         protocol.add(kHashPayloadSize, i3MPD_FIELD(MessageHeader, PayloadSize))
    );

    return protocol;
}

using SourceID_t = decltype(MessageHeader::SourceID);
constexpr SourceID_t kSourceID = 42;

} // namespace

struct MessageDispatcherTest : public ::testing::Test
{
    virtual void SetUp()
    {
        const i3::crc32_t protocolID = i3::crc32(kMagicWord.data(), kMagicWord.size());
        const i3::crc32_t sourceID = i3::crc32(kSourceID);

        EXPECT_EQ(m_Dispatcher.addProtocol<MessageHeader>().code(), i3::MessageDispatcher::ErrorCode::OK);
        EXPECT_EQ(m_Dispatcher.addProtocol<MessageHeader>().code(), i3::MessageDispatcher::ErrorCode::ProtocolDuplication);

        auto source = m_Dispatcher.addSource<i3::DataSourcePassThrough>(protocolID, sourceID);
        EXPECT_TRUE(source.isOk());

        EXPECT_EQ(m_Dispatcher.addSource<i3::DataSourcePassThrough>(protocolID, sourceID).code(), i3::MessageDispatcher::ErrorCode::SourceDuplication);

        m_DataSource = *source;

        EXPECT_EQ(protocolID, m_DataSource->getProtocolID());
        EXPECT_EQ(sourceID, m_DataSource->getSourceID());
    }

    i3::Status<i3::MessageDispatcher::ErrorCode> dispatch(std::vector<uint8_t> dumped)
    {
        const auto status = m_Dispatcher.dispatch(i3::types::bytespan{dumped.data(), dumped.size()});
        if (status.isOk())
        {
            std::copy(dumped.begin(), dumped.end(), std::back_inserter(m_Passed));
        }

        return status;
    }

    size_t read(i3::types::bytespan_mutable dest)
    {
        return m_DataSource->read(dest);
    }

    i3::MessageDispatcher::Statistics statistics()
    {
        return m_Dispatcher.statistics();
    }

protected:
    i3::MessageDispatcher m_Dispatcher{};
    std::shared_ptr<i3::DataSource> m_DataSource{};
    std::vector<uint8_t> m_Passed{};
};

TEST_F(MessageDispatcherTest, Smoke)
{
    {
        std::vector<uint8_t> dumped{};
        EXPECT_EQ(dispatch(dumped).code(), i3::MessageDispatcher::ErrorCode::ProtocolUnrecognized);

        EXPECT_EQ(statistics().MessagePassed, 0);
        EXPECT_EQ(statistics().MessageDropped, 1);
        EXPECT_EQ(statistics().MessageRefused, 0);
    }

    {
        Message m{};
        m.Header.SequenceNumber = 0;
        m.Header.SourceID = kSourceID;
        m.Header.PayloadSize = 0;

        std::vector<uint8_t> dumped;
        m.dump(dumped);

        dumped[0] = '@';

        EXPECT_EQ(dispatch(dumped).code(), i3::MessageDispatcher::ErrorCode::ProtocolUnrecognized);

        EXPECT_EQ(statistics().MessagePassed, 0);
        EXPECT_EQ(statistics().MessageDropped, 2);
        EXPECT_EQ(statistics().MessageRefused, 0);
    }

    {
        Message m{};
        m.Header.SequenceNumber = 0;
        m.Header.SourceID = kSourceID;
        m.Header.PayloadSize = 0;

        std::vector<uint8_t> dumped;
        m.dump(dumped);

        EXPECT_EQ(dispatch(dumped).code(), i3::MessageDispatcher::ErrorCode::OK);

        EXPECT_EQ(statistics().MessagePassed, 1);
        EXPECT_EQ(statistics().MessageDropped, 2);
        EXPECT_EQ(statistics().MessageRefused, 0);
    }

    {
        Message m{};
        m.Header.SequenceNumber = 0;
        m.Header.SourceID = kSourceID;

        m.Payload = std::vector<uint8_t>{'1', '2', '3', '4', '5'};
        m.Header.PayloadSize = static_cast<int16_t>(m.Payload.size());

        std::vector<uint8_t> dumped;
        m.dump(dumped);

        EXPECT_EQ(dispatch(dumped).code(), i3::MessageDispatcher::ErrorCode::OK);

        EXPECT_EQ(statistics().MessagePassed, 2);
        EXPECT_EQ(statistics().MessageDropped, 2);
        EXPECT_EQ(statistics().MessageRefused, 0);
    }

    {
        Message m{};
        m.Header.SequenceNumber = 0;
        m.Header.SourceID = kSourceID;

        m.Payload = std::vector<uint8_t>{'1', '2', '3', '4', '5'};
        m.Header.PayloadSize = static_cast<int16_t>(m.Payload.size());

        std::vector<uint8_t> dumped;
        m.dump(dumped);

        EXPECT_EQ(dispatch(dumped).code(), i3::MessageDispatcher::ErrorCode::OK);

        EXPECT_EQ(statistics().MessagePassed, 3);
        EXPECT_EQ(statistics().MessageDropped, 2);
        EXPECT_EQ(statistics().MessageRefused, 0);
    }

    {
        std::vector<uint8_t> buffer{};
        buffer.resize(4096);

        auto ret = read({buffer.data(), buffer.size()});
        EXPECT_EQ(ret, sizeof(MessageHeader) * 3 + 10);

        buffer.resize(ret);
        EXPECT_EQ(buffer, m_Passed);

        m_Passed.clear();
    }

    {
        Message m{};
        m.Header.SequenceNumber = 0;
        m.Header.SourceID = kSourceID;

        m.Payload = std::vector<uint8_t>{'1', '2', '3', '4', '5'};
        m.Header.PayloadSize = static_cast<int16_t>(m.Payload.size());

        std::vector<uint8_t> dumped;
        m.dump(dumped);

        EXPECT_EQ(dispatch(dumped).code(), i3::MessageDispatcher::ErrorCode::OK);

        EXPECT_EQ(statistics().MessagePassed, 4);
        EXPECT_EQ(statistics().MessageDropped, 2);
        EXPECT_EQ(statistics().MessageRefused, 0);
    }

    {
        Message m{};
        m.Header.SequenceNumber = 0;
        m.Header.SourceID = kSourceID + 1;

        m.Payload = std::vector<uint8_t>{'1', '2', '3', '4', '5'};
        m.Header.PayloadSize = static_cast<int16_t>(m.Payload.size());

        std::vector<uint8_t> dumped;
        m.dump(dumped);

        EXPECT_EQ(dispatch(dumped).code(), i3::MessageDispatcher::ErrorCode::SourceNotFound);

        EXPECT_EQ(statistics().MessagePassed, 4);
        EXPECT_EQ(statistics().MessageDropped, 3);
        EXPECT_EQ(statistics().MessageRefused, 0);
    }


    {
        std::vector<uint8_t> buffer{};
        buffer.resize(4096);

        auto ret = read({buffer.data(), buffer.size()});
        EXPECT_EQ(ret, sizeof(MessageHeader) + 5);

        buffer.resize(ret);
        EXPECT_EQ(buffer, m_Passed);

        m_Passed.clear();
    }
}
