#include <gtest/gtest.h>

#include "I3/Common/CRC32.h"

TEST(CRC32, Zero)
{
    const char* str = "";

    const i3::crc32_t hash = i3::crc32(str, strlen(str));

    EXPECT_EQ(hash, 0x0u);
}

TEST(CRC32, LoremIpsum)
{
    const char* str = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, "
                      "sed do eiusmod tempor incididunt ut labore et dolore magna "
                      "aliqua. Ut enim ad minim veniam, quis nostrud exercitation "
                      "ullamco laboris nisi ut aliquip ex ea commodo consequat. "
                      "Duis aute irure dolor in reprehenderit in voluptate velit "
                      "esse cillum dolore eu fugiat nulla pariatur. Excepteur sint "
                      "occaecat cupidatat non proident, sunt in culpa qui officia "
                      "deserunt mollit anim id est laborum.";

    const i3::crc32_t hash = i3::crc32(str, strlen(str));
    EXPECT_EQ(hash, 0x98b2c5bdu);
}

TEST(CRC32, CompileTime)
{
    const char* str = "42";

    const i3::crc32_t hash0 = i3::crc32(str, strlen(str));
    const i3::crc32_t hash1 = i3CRC32_CompileTime(42);

    EXPECT_EQ(hash0, 0x3224b088u);
    EXPECT_EQ(hash1, 0x3224b088u);
}
