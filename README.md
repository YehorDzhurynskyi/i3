# I3

Poll-based Asynchronous IO Library

## Build

Run the following commands from root of project

> cmake -S . -B <build dir\>  
> cmake --build <build dir\> --config release

## Usage

There are several configurations that could be picked via command line arguments:
 1. `sssp` - Single Source Single Provider (10Mb - 100Mb)
 2. `sssp_cbr` - Signle Source Single Provider (200Mb)
 3. `sssp_corrupted` - Signle Source Single Provider With Corrupted Data (100 Mb)
 4. `ssmp` - Single Source Multiple Providers (10Mb - 100Mb & 100Mb)

 **NOTE**: `sssp` configuration will be picked with no command line arguments by default

### Example

The following example will call `ssmp` configuration

 > <build dir\>\I3v0Demo\Release\I3v0-Demo.exe ssmp

### Output Description

The following message will be printed every second

```
bitrate in     45 MB 529 KB 76 B
bitrate out    45 MB 526 KB 676 B
bitrate read   815 KB 104 B
data sources   1
msg passed     200
msg dropped    0
msg refused    0
msg pass rate  100%
buffer         4 KB 96 B
```

`bitrate in` - is the number of bits that are conveyed by data providers

`bitrate out` - is the number of bits that are processed by data sources

`bitrate read` - is the number of bits that are conveyed by the user on **read()** call

`data sources` - is the number of data sources

`msg passed` - is the number of messages that have been successfully processed and passed to the user

`msg dropped` - is the number of messages that have been considered invalid and as a result, dropped

`msg refused` - is the number of messages that have been considered corrupted (in terms of business logic) and as a result, refused

`msg pass rate` - is the percentage of passed messages compared to the total count

`buffer` - is the size of the buffer used for intermediate storing while calling **read()**

## Architecture

### Structure Diagram

![Structure Diagram](./doc/StructureDiagram.jpg)

### Class Diagram

![Class Diagram](./doc/ClassDiagram.jpg)
