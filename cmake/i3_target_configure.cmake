function(i3_target_configure TARGET)
    set_target_properties(${TARGET}
        PROPERTIES
            CXX_STANDARD 17
            CXX_STANDARD_REQUIRED YES
            CXX_EXTENSIONS NO
    )

    if (MSVC)
        target_compile_options(${TARGET}
            PRIVATE
                "/W4"                  # extended warning list
                "/WX"                  # treat warning as error
                "/GR-"                 # turn off RTTI
                "/EHsc"                # needed to turn off exceptions
                "/permissive-"         # to make code both more correct and more portable
                "/Zc:referenceBinding" # see https://docs.microsoft.com/en-us/cpp/build/reference/zc-referencebinding-enforce-reference-binding-rules?view=msvc-160
                "/Zc:rvalueCast"       # see https://docs.microsoft.com/en-us/cpp/build/reference/zc-rvaluecast-enforce-type-conversion-rules?view=msvc-160
        )

        target_compile_definitions(${TARGET}
            PRIVATE
                "_HAS_EXCEPTIONS=0"    # turn off exceptions for STL
                _USE_MATH_DEFINES
        )
    endif()

    get_target_property(${TARGET}_SOURCES ${TARGET} SOURCES)
    i3_pack_sources(${${TARGET}_SOURCES})
endfunction()
