#include <gtest/gtest.h>

#include "I3v0/DataSource/DataSourceMessageI3v0.h"

namespace
{

using SourceID_t = decltype(i3::MessageI3v0_Header::SourceID);
constexpr SourceID_t kSourceID = 42;

} // namespace

class MockDataSourceMessageI3v0 : public i3::DataSourceMessageI3v0
{
public:
    MockDataSourceMessageI3v0()
        : DataSourceMessageI3v0{i3::crc32(kSourceID)}
    {
    }

    i3::types::bytespan processMessage(const i3::MessageI3v0& msg)
    {
        return i3::DataSourceMessageI3v0::processMessage(msg);
    }

    bool validateMessage(const i3::MessageI3v0& msg) const override
    {
        return i3::DataSourceMessageI3v0::validateMessage(msg);
    }
};

TEST(DataSourceMessageI3v0, Smoke)
{
    MockDataSourceMessageI3v0 source{};

    EXPECT_EQ(source.getSourceID(), i3::crc32(kSourceID));

    i3::types::bytespan magicword{i3::MessageI3v0_Header::kMagicWord.data(), i3::MessageI3v0_Header::kMagicWord.size()};

    EXPECT_EQ(source.getProtocolID(), i3::crc32(magicword.data(), magicword.size()));
    EXPECT_EQ(source.getSourceID(), i3::crc32(kSourceID));
}

TEST(DataSourceMessageI3v0, ValidateMessage)
{
    MockDataSourceMessageI3v0 source{};

    {
        i3::MessageI3v0 m{};

        EXPECT_TRUE(source.validateMessage(m));
    }

    {
        i3::MessageI3v0 m{};
        m.Header.SequenceNumber = 42;
        m.Header.SourceID = 13;
        m.Header.PayloadType = i3::MessageI3v0_DataType::Float32;
        m.Payload = std::vector<uint8_t>{'1', '2', '3', '4', '5'};
        m.Header.PayloadSize = static_cast<decltype(i3::MessageI3v0_Header::PayloadSize)>(m.Payload.size());

        EXPECT_TRUE(source.validateMessage(m));
    }

    {
        i3::MessageI3v0 m{};
        m.Header.PayloadType = static_cast<i3::MessageI3v0_DataType>(-42);

        EXPECT_FALSE(source.validateMessage(m));
    }
}

TEST(DataSourceMessageI3v0, ProcessMessageDefault)
{
    MockDataSourceMessageI3v0 source{};

    {
        i3::MessageI3v0 m{};

        auto res = source.processMessage(m);
        EXPECT_EQ(res.size(), 0);
    }
}

namespace
{

template<typename T>
std::vector<uint8_t> MakePayloadAscending(int32_t payloadElements, int32_t offset)
{
    std::vector<uint8_t> payload{};
    payload.resize(payloadElements * sizeof(T));

    for (int32_t i = 0, v = offset; i < payloadElements; ++i, ++v)
    {
        const T value = static_cast<T>(v);
        memcpy(payload.data() + i * sizeof(T), &value, sizeof(T));
    }

    return payload;
}

std::vector<uint8_t> MakePayloadAscending(i3::MessageI3v0_DataType type, int32_t payloadElements, int32_t offset)
{
    switch (type)
    {
    case i3::MessageI3v0_DataType::UInt8:
    {
        return MakePayloadAscending<uint8_t>(payloadElements, 0);
    }
    case i3::MessageI3v0_DataType::Int16:
    {
        return MakePayloadAscending<int16_t>(payloadElements, offset);
    }
    case i3::MessageI3v0_DataType::Int32:
    {
        return MakePayloadAscending<int32_t>(payloadElements, offset);
    }
    case i3::MessageI3v0_DataType::Float32:
    {
        return MakePayloadAscending<float>(payloadElements, offset);
    }
    }

    i3LogFatal("Unexpected type=`%i`!", static_cast<int32_t>(type));

    return std::vector<uint8_t>{};
}

template<typename T>
std::vector<uint8_t> MakePayloadSame(int32_t payloadElements)
{
    std::vector<uint8_t> payload{};
    payload.resize(payloadElements * sizeof(T));

    for (int32_t i = 0; i < payloadElements; ++i)
    {
        const T value = static_cast<T>(5);
        memcpy(payload.data() + i * sizeof(T), &value, sizeof(T));
    }

    return payload;
}

std::vector<uint8_t> MakePayloadSame(i3::MessageI3v0_DataType type, int32_t payloadElements)
{
    switch (type)
    {
    case i3::MessageI3v0_DataType::UInt8:
    {
        return MakePayloadSame<uint8_t>(payloadElements);
    }
    case i3::MessageI3v0_DataType::Int16:
    {
        return MakePayloadSame<int16_t>(payloadElements);
    }
    case i3::MessageI3v0_DataType::Int32:
    {
        return MakePayloadSame<int32_t>(payloadElements);
    }
    case i3::MessageI3v0_DataType::Float32:
    {
        return MakePayloadSame<float>(payloadElements);
    }
    }

    i3LogFatal("Unexpected type=`%i`!", static_cast<int32_t>(type));

    return std::vector<uint8_t>{};
}

} // namespace

TEST(DataSourceMessageI3v0, ProcessMessagePayloadedAscending)
{
    const std::vector<i3::MessageI3v0_DataType> types
    {
        i3::MessageI3v0_DataType::UInt8,
        i3::MessageI3v0_DataType::Int16,
        i3::MessageI3v0_DataType::Int32,
        i3::MessageI3v0_DataType::Float32,
    };

    constexpr int32_t kPayloadElements{5};
    constexpr int32_t kOffsetSigned{-2};

    for (auto type : types)
    {
        MockDataSourceMessageI3v0 source{};

        i3::MessageI3v0 m{};
        m.Header.PayloadType = type;
        m.Payload = MakePayloadAscending(type, kPayloadElements, kOffsetSigned);
        m.Header.PayloadSize = static_cast<decltype(i3::MessageI3v0_Header::PayloadSize)>(m.Payload.size());

        auto res = source.processMessage(m);
        EXPECT_EQ(res.size(), kPayloadElements * sizeof(float));

        const float* data = reinterpret_cast<const float*>(res.data());
        for (int32_t i = 0; i < kPayloadElements; ++i)
        {
            float v = i / static_cast<float>(kPayloadElements - 1);
            v = v * 2.0f - 1.0f;

            EXPECT_EQ(data[i], v);
        }
    }
}

TEST(DataSourceMessageI3v0, ProcessMessagePayloadedSame)
{
    const std::vector<i3::MessageI3v0_DataType> types
    {
        i3::MessageI3v0_DataType::UInt8,
        i3::MessageI3v0_DataType::Int16,
        i3::MessageI3v0_DataType::Int32,
        i3::MessageI3v0_DataType::Float32,
    };

    constexpr int32_t kPayloadElements{5};

    for (auto type : types)
    {
        MockDataSourceMessageI3v0 source{};

        i3::MessageI3v0 m{};
        m.Header.PayloadType = type;
        m.Payload = MakePayloadSame(type, kPayloadElements);
        m.Header.PayloadSize = static_cast<decltype(i3::MessageI3v0_Header::PayloadSize)>(m.Payload.size());

        auto res = source.processMessage(m);
        EXPECT_EQ(res.size(), kPayloadElements * sizeof(float));

        const float* data = reinterpret_cast<const float*>(res.data());
        for (int32_t i = 0; i < kPayloadElements; ++i)
        {
            EXPECT_EQ(data[i], 0.0f);
        }
    }
}

TEST(DataSourceMessageI3v0, ProcessMessageWrongByteGranularity)
{
    const std::vector<i3::MessageI3v0_DataType> types
    {
        i3::MessageI3v0_DataType::UInt8,
        i3::MessageI3v0_DataType::Int16,
        i3::MessageI3v0_DataType::Int32,
        i3::MessageI3v0_DataType::Float32,
    };

    constexpr int32_t kPayloadElements{5};
    constexpr int32_t kOffsetSigned{-2};

    for (auto type : types)
    {
        MockDataSourceMessageI3v0 source{};

        i3::MessageI3v0 m{};
        m.Header.PayloadType = type;
        m.Payload = MakePayloadAscending(type, kPayloadElements, kOffsetSigned);
        m.Header.PayloadSize = static_cast<decltype(i3::MessageI3v0_Header::PayloadSize)>(m.Payload.size() - 1);

        auto res = source.processMessage(m);

        EXPECT_EQ(res.size(), (kPayloadElements - 1) * sizeof(float));

        const float* data = reinterpret_cast<const float*>(res.data());
        for (int32_t i = 0; i < (kPayloadElements - 1); ++i)
        {
            if (type == i3::MessageI3v0_DataType::UInt8)
            {
                float v = i / static_cast<float>(kPayloadElements - 1 - 1);
                v = v * 2.0f - 1.0f;

                EXPECT_EQ(data[i], v);
            }
            else
            {
                EXPECT_EQ(data[i], 0.0f);
            }
        }
    }
}

TEST(DataSourceMessageI3v0, ProcessMessageOutOfOrder)
{
    const std::vector<i3::MessageI3v0_DataType> types
    {
        i3::MessageI3v0_DataType::UInt8,
        i3::MessageI3v0_DataType::Int16,
        i3::MessageI3v0_DataType::Int32,
        i3::MessageI3v0_DataType::Float32,
    };

    constexpr int32_t kPayloadElements{5};
    constexpr int32_t kOffsetSigned{-2};
    constexpr decltype(i3::MessageI3v0_Header::SequenceNumber) kSequenceNumber{2};

    for (auto type : types)
    {
        MockDataSourceMessageI3v0 source{};

        i3::MessageI3v0 m{};
        m.Header.PayloadType = type;
        m.Header.SequenceNumber = kSequenceNumber;
        m.Payload = MakePayloadAscending(type, kPayloadElements, kOffsetSigned);
        m.Header.PayloadSize = static_cast<decltype(i3::MessageI3v0_Header::PayloadSize)>(m.Payload.size() - 1);

        auto res = source.processMessage(m);

        auto expectedPayloadElements = kPayloadElements - 1;

        EXPECT_EQ(res.size(), (expectedPayloadElements + expectedPayloadElements * kSequenceNumber) * sizeof(float));

        const float* data = reinterpret_cast<const float*>(res.data());
        for (int32_t i = 0; i < expectedPayloadElements * kSequenceNumber; ++i)
        {
            EXPECT_EQ(data[i], 0.0f);
        }

        for (int32_t i = 0; i < expectedPayloadElements; ++i)
        {
            if (type == i3::MessageI3v0_DataType::UInt8)
            {
                float v = i / static_cast<float>(expectedPayloadElements - 1);
                v = v * 2.0f - 1.0f;

                EXPECT_EQ(data[i + expectedPayloadElements * kSequenceNumber], v);
            }
            else
            {
                EXPECT_EQ(data[i + expectedPayloadElements * kSequenceNumber], 0.0f);
            }
        }
    }
}

TEST(DataSourceMessageI3v0, Push)
{
    MockDataSourceMessageI3v0 source{};

    constexpr i3::MessageI3v0_DataType kPayloadType{i3::MessageI3v0_DataType::Int32};
    constexpr int32_t kPayloadElements{5};
    constexpr int32_t kOffsetSigned{-2};

    {
        i3::MessageI3v0 m{};
        m.Header.PayloadType = static_cast<i3::MessageI3v0_DataType>(-3);
        m.Header.SequenceNumber = 0;
        m.Payload = MakePayloadAscending(kPayloadType, kPayloadElements, kOffsetSigned);
        m.Header.PayloadSize = static_cast<decltype(i3::MessageI3v0_Header::PayloadSize)>(m.Payload.size());

        std::vector<uint8_t> dumped{};
        i3::ProtocolStatus status = m.dump(dumped);
        EXPECT_TRUE(status.isOk());

        i3::types::bytespan message{dumped.data(), dumped.size()};
        EXPECT_FALSE(source.push(message));
    }

    {
        i3::MessageI3v0 m{};
        m.Header.PayloadType = kPayloadType;
        m.Header.SequenceNumber = 0;
        m.Payload = MakePayloadAscending(kPayloadType, kPayloadElements, kOffsetSigned);
        m.Header.PayloadSize = static_cast<decltype(i3::MessageI3v0_Header::PayloadSize)>(m.Payload.size());

        std::vector<uint8_t> dumped{};
        i3::ProtocolStatus status = m.dump(dumped);
        EXPECT_TRUE(status.isOk());

        ((i3::MessageI3v0_Header*)dumped.data())->PayloadSize = -42;

        i3::types::bytespan message{dumped.data(), dumped.size()};
        EXPECT_FALSE(source.push(message));
    }

    {
        i3::MessageI3v0 m{};
        m.Header.PayloadType = kPayloadType;
        m.Header.SequenceNumber = 0;
        m.Payload = MakePayloadAscending(kPayloadType, kPayloadElements, kOffsetSigned);
        m.Header.PayloadSize = static_cast<decltype(i3::MessageI3v0_Header::PayloadSize)>(m.Payload.size());

        std::vector<uint8_t> dumped{};
        i3::ProtocolStatus status = m.dump(dumped);
        EXPECT_TRUE(status.isOk());

        i3::types::bytespan message{dumped.data(), dumped.size()};
        EXPECT_TRUE(source.push(message));
    }
}

TEST(DataSourceMessageI3v0, PushRead)
{
    MockDataSourceMessageI3v0 source{};

    constexpr i3::MessageI3v0_DataType kPayloadType{i3::MessageI3v0_DataType::Int32};
    constexpr int32_t kPayloadElements{5};
    constexpr int32_t kOffsetSigned{-2};

    std::vector<uint8_t> buffer{};
    buffer.resize(13);

    i3::types::bytespan_mutable dest{buffer.data(), buffer.size()};

    EXPECT_EQ(source.read(dest), 0);

    {
        i3::MessageI3v0 m{};
        m.Header.PayloadType = static_cast<i3::MessageI3v0_DataType>(-3);
        m.Header.SequenceNumber = 0;
        m.Payload = MakePayloadAscending(kPayloadType, kPayloadElements, kOffsetSigned);
        m.Header.PayloadSize = static_cast<decltype(i3::MessageI3v0_Header::PayloadSize)>(m.Payload.size());

        std::vector<uint8_t> dumped{};
        i3::ProtocolStatus status = m.dump(dumped);
        EXPECT_TRUE(status.isOk());

        i3::types::bytespan message{dumped.data(), dumped.size()};
        EXPECT_FALSE(source.push(message));
    }

    {
        i3::MessageI3v0 m{};
        m.Header.PayloadType = kPayloadType;
        m.Header.SequenceNumber = 0;
        m.Payload = MakePayloadAscending(kPayloadType, kPayloadElements, kOffsetSigned);
        m.Header.PayloadSize = static_cast<decltype(i3::MessageI3v0_Header::PayloadSize)>(m.Payload.size());

        std::vector<uint8_t> dumped{};
        i3::ProtocolStatus status = m.dump(dumped);
        EXPECT_TRUE(status.isOk());

        ((i3::MessageI3v0_Header*)dumped.data())->PayloadSize = -42;

        i3::types::bytespan message{dumped.data(), dumped.size()};
        EXPECT_FALSE(source.push(message));
    }

    EXPECT_EQ(source.read(dest), 0);

    {
        i3::MessageI3v0 m{};
        m.Header.PayloadType = kPayloadType;
        m.Header.SequenceNumber = 0;
        m.Payload = MakePayloadAscending(kPayloadType, kPayloadElements, kOffsetSigned);
        m.Header.PayloadSize = static_cast<decltype(i3::MessageI3v0_Header::PayloadSize)>(m.Payload.size());

        std::vector<uint8_t> dumped{};
        i3::ProtocolStatus status = m.dump(dumped);
        EXPECT_TRUE(status.isOk());

        i3::types::bytespan message{dumped.data(), dumped.size()};
        EXPECT_TRUE(source.push(message));
    }

    {
        i3::MessageI3v0 m{};
        m.Header.PayloadType = i3::MessageI3v0_DataType::Int16;
        m.Header.SequenceNumber = 3;
        m.Payload = MakePayloadAscending(i3::MessageI3v0_DataType::Int16, 100, 0);
        m.Header.PayloadSize = static_cast<decltype(i3::MessageI3v0_Header::PayloadSize)>(m.Payload.size());

        std::vector<uint8_t> dumped{};
        i3::ProtocolStatus status = m.dump(dumped);
        EXPECT_TRUE(status.isOk());

        i3::types::bytespan message{dumped.data(), dumped.size()};
        EXPECT_TRUE(source.push(message));
    }

    std::vector<uint8_t> expected{};
    expected.reserve(1220);

    for (int32_t i = 0; i < kPayloadElements; ++i)
    {
        float v = i / static_cast<float>(kPayloadElements - 1);
        v = v * 2.0f - 1.0f;

        uint8_t buff[sizeof(float)];
        memcpy_s(buff, sizeof(buff), &v, sizeof(float));

        std::copy_n(buff, sizeof(buff), std::back_inserter(expected));
    }

    for (int32_t i = 0; i < 800; ++i)
    {
        expected.push_back(0x0);
    }

    for (int32_t i = 0; i < 100; ++i)
    {
        float v = i / static_cast<float>(100 - 1);
        v = v * 2.0f - 1.0f;

        uint8_t buff[sizeof(float)];
        memcpy_s(buff, sizeof(buff), &v, sizeof(float));

        std::copy_n(buff, sizeof(buff), std::back_inserter(expected));
    }

    auto startAt = std::chrono::steady_clock::now();

    std::vector<uint8_t> actual{};
    while (true)
    {
        // we expect that it's enough to wait such time
        // to give a chance to process all pushed messages
        auto now = std::chrono::steady_clock::now();
        if (now - startAt >= std::chrono::seconds(1))
        {
            break;
        }

        const size_t ret = source.read(dest);
        std::copy_n(dest.begin(), ret, std::back_inserter(actual));
    }

    EXPECT_EQ(source.read(dest), 0);
    EXPECT_EQ(actual, expected);
}

TEST(DataSourceMessageI3v0, StressTest)
{
    MockDataSourceMessageI3v0 source{};

    constexpr i3::MessageI3v0_DataType kPayloadType{i3::MessageI3v0_DataType::Int16};
    constexpr int32_t kPayloadElements{100};
    constexpr int32_t kOffsetSigned{-11};

    std::vector<uint8_t> buffer{};
    buffer.resize(3333);

    i3::types::bytespan_mutable dest{buffer.data(), buffer.size()};

    EXPECT_EQ(source.read(dest), 0);

    constexpr int32_t kIterations{70'000};

    for (int32_t i = 0; i < kIterations; ++i)
    {
        i3::MessageI3v0 m{};
        m.Header.PayloadType = kPayloadType;
        m.Header.SequenceNumber = static_cast<decltype(i3::MessageI3v0_Header::SequenceNumber)>(i);
        m.Payload = MakePayloadAscending(kPayloadType, kPayloadElements, kOffsetSigned);
        m.Header.PayloadSize = static_cast<decltype(i3::MessageI3v0_Header::PayloadSize)>(m.Payload.size());

        std::vector<uint8_t> dumped{};
        i3::ProtocolStatus status = m.dump(dumped);
        EXPECT_TRUE(status.isOk());

        i3::types::bytespan message{dumped.data(), dumped.size()};
        EXPECT_TRUE(source.push(message));
    }

    std::vector<uint8_t> expected{};
    expected.reserve(kIterations * kPayloadElements * sizeof(float));

    for (int32_t j = 0; j < kIterations; ++j)
    {
        for (int32_t i = 0; i < kPayloadElements; ++i)
        {
            float v = i / static_cast<float>(kPayloadElements - 1);
            v = v * 2.0f - 1.0f;

            uint8_t buff[sizeof(float)];
            memcpy_s(buff, sizeof(buff), &v, sizeof(float));

            std::copy_n(buff, sizeof(buff), std::back_inserter(expected));
        }
    }

    auto startAt = std::chrono::steady_clock::now();

    std::vector<uint8_t> actual{};
    while (true)
    {
        // we expect that it's enough to wait such time
        // to give a chance to process all pushed messages
        auto now = std::chrono::steady_clock::now();
        if (now - startAt >= std::chrono::seconds(6))
        {
            break;
        }

        const size_t ret = source.read(dest);
        std::copy_n(dest.begin(), ret, std::back_inserter(actual));
    }

    EXPECT_EQ(source.read(dest), 0);

    for (int32_t i = 0; i < actual.size(); ++i)
    {
        i3LogAssert_BreakPoint(actual[i] == expected[i]);
    }

    EXPECT_EQ(actual, expected);
}

