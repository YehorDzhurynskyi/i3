#include <gtest/gtest.h>

#include "I3/Common/Clock.h"
#include "I3/Metric/MetricGeneratorCBR.h"
#include "I3v0/Message/MessageI3v0.h"
#include "I3v0/DataProvider/DataProviderGeneratorI3v0.h"

#include <chrono>

using namespace i3::literals;

TEST(DataProviderGeneratorI3v0, BitrateZero)
{
    using namespace std::chrono_literals;

    i3::Clock::reset();

    constexpr int32_t kBitrate = 0;
    constexpr decltype(i3::MessageI3v0_Header::SourceID) kSourceID = 42;

    i3::DataProviderGeneratorI3v0::Config cfg{};
    cfg.MetricBitrate = std::make_unique<i3::MetricGeneratorCBR>(kBitrate);
    cfg.SourceID = kSourceID;

    i3::DataProviderGeneratorI3v0 provider{std::move(cfg)};

    {
        i3::Result<i3::types::bytespan, i3::DataProvider::ErrorCode> chunk = provider.read();

        EXPECT_EQ(chunk.code(), i3::DataProvider::ErrorCode::EmptyChunk);
    }

    i3::Clock::advance(100ms);

    {
        i3::Result<i3::types::bytespan, i3::DataProvider::ErrorCode> chunk = provider.read();

        EXPECT_EQ(chunk.code(), i3::DataProvider::ErrorCode::EmptyChunk);
    }

    i3::Clock::advance(1000ms);

    {
        i3::Result<i3::types::bytespan, i3::DataProvider::ErrorCode> chunk = provider.read();

        EXPECT_EQ(chunk.code(), i3::DataProvider::ErrorCode::EmptyChunk);
    }
}

TEST(DataProviderGeneratorI3v0, BitrateSizeOfHalfHeader)
{
    using namespace std::chrono_literals;

    i3::Clock::reset();

    constexpr int32_t kBitrate = static_cast<int32_t>((sizeof(i3::MessageI3v0_Header) / 2) * 8);
    constexpr decltype(i3::MessageI3v0_Header::SourceID) kSourceID = 42;

    i3::DataProviderGeneratorI3v0::Config cfg{};
    cfg.MetricBitrate = std::make_unique<i3::MetricGeneratorCBR>(kBitrate);
    cfg.SourceID = kSourceID;

    i3::DataProviderGeneratorI3v0 provider{std::move(cfg)};

    {
        i3::Result<i3::types::bytespan, i3::DataProvider::ErrorCode> chunk = provider.read();

        EXPECT_EQ(chunk.code(), i3::DataProvider::ErrorCode::EmptyChunk);
    }

    i3::Clock::advance(100ms);

    {
        i3::Result<i3::types::bytespan, i3::DataProvider::ErrorCode> chunk = provider.read();

        EXPECT_EQ(chunk.code(), i3::DataProvider::ErrorCode::EmptyChunk);
    }

    i3::Clock::advance(1000ms);

    {
        i3::Result<i3::types::bytespan, i3::DataProvider::ErrorCode> chunk = provider.read();

        EXPECT_EQ(chunk.code(), i3::DataProvider::ErrorCode::EmptyChunk);
    }
}

TEST(DataProviderGeneratorI3v0, BitrateSizeOfHeader)
{
    using namespace std::chrono_literals;

    i3::Clock::reset();

    constexpr int32_t kBitrate = static_cast<int32_t>(sizeof(i3::MessageI3v0_Header) * 8);
    constexpr decltype(i3::MessageI3v0_Header::SourceID) kSourceID = 42;

    i3::DataProviderGeneratorI3v0::Config cfg{};
    cfg.MetricBitrate = std::make_unique<i3::MetricGeneratorCBR>(kBitrate);
    cfg.SourceID = kSourceID;

    i3::DataProviderGeneratorI3v0 provider{std::move(cfg)};

    {
        i3::Result<i3::types::bytespan, i3::DataProvider::ErrorCode> chunk = provider.read();

        EXPECT_EQ(chunk.code(), i3::DataProvider::ErrorCode::EmptyChunk);
    }

    for (int32_t i = 0; i < 9; ++i)
    {
        i3::Clock::advance(100ms);

        {
            i3::Result<i3::types::bytespan, i3::DataProvider::ErrorCode> chunk = provider.read();

            EXPECT_EQ(chunk.code(), i3::DataProvider::ErrorCode::EmptyChunk);
        }
    }

    i3::Clock::advance(999ms);

    {
        i3::Result<i3::types::bytespan, i3::DataProvider::ErrorCode> chunk = provider.read();

        EXPECT_EQ(chunk.code(), i3::DataProvider::ErrorCode::EmptyChunk);
    }

    for (int32_t i = 0; i < 5; ++i)
    {
        i3::Clock::advance(1000ms);

        {
            i3::Result<i3::types::bytespan, i3::DataProvider::ErrorCode> chunk = provider.read();

            EXPECT_TRUE(chunk.isOk());
            EXPECT_EQ(chunk->size(), sizeof(i3::MessageI3v0_Header));

            i3::MessageI3v0 msg{};
            i3::ProtocolStatus status = msg.parse(*chunk);
            EXPECT_TRUE(status.isOk());

            EXPECT_EQ(msg.Header.SequenceNumber, i);
            EXPECT_EQ(msg.Header.SourceID, kSourceID);
            EXPECT_EQ(msg.Header.PayloadType, i3::MessageI3v0_DataType::UInt8);
            EXPECT_EQ(msg.Header.PayloadSize, 0);

            const std::vector<uint8_t> payloadExpected{};
            EXPECT_EQ(msg.Payload, payloadExpected);
        }
    }
}

TEST(DataProviderGeneratorI3v0, BitrateSizeOfHeaderPlusOne)
{
    using namespace std::chrono_literals;

    const std::vector<i3::MessageI3v0_DataType> dataTypes{
        i3::MessageI3v0_DataType::UInt8,
        i3::MessageI3v0_DataType::Int16,
        i3::MessageI3v0_DataType::Int32,
        i3::MessageI3v0_DataType::Float32,
    };

    for (auto datatype : dataTypes)
    {
        i3::Clock::reset();

        constexpr int32_t kBitrate = static_cast<int32_t>((sizeof(i3::MessageI3v0_Header) + 1) * 8);
        constexpr decltype(i3::MessageI3v0_Header::SourceID) kSourceID = 42;

        i3::DataProviderGeneratorI3v0::Config cfg{};
        cfg.MetricBitrate = std::make_unique<i3::MetricGeneratorCBR>(kBitrate);
        cfg.SourceID = kSourceID;
        cfg.PayloadType = datatype;

        i3::DataProviderGeneratorI3v0 provider{std::move(cfg)};

        i3::Result<i3::types::bytespan, i3::DataProvider::ErrorCode> chunk0 = provider.read();
        EXPECT_EQ(chunk0.code(), i3::DataProvider::ErrorCode::EmptyChunk);

        for (int32_t i = 0; i < 5; ++i)
        {
            i3::Clock::advance(1000ms);

            {
                i3::Result<i3::types::bytespan, i3::DataProvider::ErrorCode> chunk = provider.read();

                EXPECT_TRUE(chunk.isOk());
                EXPECT_EQ(chunk->size(), sizeof(i3::MessageI3v0_Header) + i3::SizeOf(datatype));

                i3::MessageI3v0 msg{};
                i3::ProtocolStatus status = msg.parse(*chunk);
                EXPECT_TRUE(status.isOk());

                EXPECT_EQ(msg.Header.SequenceNumber, i);
                EXPECT_EQ(msg.Header.SourceID, kSourceID);
                EXPECT_EQ(msg.Header.PayloadType, datatype);
                EXPECT_EQ(msg.Header.PayloadSize, i3::SizeOf(datatype));

                std::vector<uint8_t> payloadExpected{};
                payloadExpected.resize(i3::SizeOf(datatype), 0x0);

                EXPECT_EQ(msg.Payload, payloadExpected);
            }
        }
    }
}

TEST(DataProviderGeneratorI3v0, PayloadPattern)
{
    using namespace std::chrono_literals;

    i3::Clock::reset();

    constexpr uint32_t kPayloadElementCount{5};

    const std::vector<i3::MessageI3v0_DataType> dataTypes{
        i3::MessageI3v0_DataType::UInt8,
        i3::MessageI3v0_DataType::Int16,
        i3::MessageI3v0_DataType::Int32,
        i3::MessageI3v0_DataType::Float32,
    };

    const std::vector<i3::DataProviderGeneratorI3v0::Pattern> patterns{
        i3::DataProviderGeneratorI3v0::Pattern::Zero,
        i3::DataProviderGeneratorI3v0::Pattern::FF,
        i3::DataProviderGeneratorI3v0::Pattern::Ascending,
    };

    using Patterns_t = std::map<i3::DataProviderGeneratorI3v0::Pattern, std::vector<uint8_t>>;
    std::map<i3::MessageI3v0_DataType, Patterns_t> expectedPayloads;

    expectedPayloads[i3::MessageI3v0_DataType::UInt8] = Patterns_t{
        {
            i3::DataProviderGeneratorI3v0::Pattern::Zero,
            std::vector<uint8_t>{0x0, 0x0, 0x0, 0x0, 0x0}
        },
        {
            i3::DataProviderGeneratorI3v0::Pattern::FF,
            std::vector<uint8_t>{0xff, 0xff, 0xff, 0xff, 0xff}
        },
        {
            i3::DataProviderGeneratorI3v0::Pattern::Ascending,
            std::vector<uint8_t>{0x0, 0x1, 0x2, 0x3, 0x4}
        },
    };

    expectedPayloads[i3::MessageI3v0_DataType::Int16] = Patterns_t{
        {
            i3::DataProviderGeneratorI3v0::Pattern::Zero,
            std::vector<uint8_t>{0x0, 0x0,
                                 0x0, 0x0,
                                 0x0, 0x0,
                                 0x0, 0x0,
                                 0x0, 0x0}
        },
        {
            i3::DataProviderGeneratorI3v0::Pattern::FF,
            std::vector<uint8_t>{0xff, 0xff,
                                 0xff, 0xff,
                                 0xff, 0xff,
                                 0xff, 0xff,
                                 0xff, 0xff}
        },
        {
            i3::DataProviderGeneratorI3v0::Pattern::Ascending,
            std::vector<uint8_t>{0x0, 0x0,
                                 0x1, 0x0,
                                 0x2, 0x0,
                                 0x3, 0x0,
                                 0x4, 0x0}
        },
    };

    expectedPayloads[i3::MessageI3v0_DataType::Int32] = Patterns_t{
        {
            i3::DataProviderGeneratorI3v0::Pattern::Zero,
            std::vector<uint8_t>{0x0, 0x0, 0x0, 0x0,
                                 0x0, 0x0, 0x0, 0x0,
                                 0x0, 0x0, 0x0, 0x0,
                                 0x0, 0x0, 0x0, 0x0,
                                 0x0, 0x0, 0x0, 0x0}
        },
        {
            i3::DataProviderGeneratorI3v0::Pattern::FF,
            std::vector<uint8_t>{0xff, 0xff, 0xff, 0xff,
                                 0xff, 0xff, 0xff, 0xff,
                                 0xff, 0xff, 0xff, 0xff,
                                 0xff, 0xff, 0xff, 0xff,
                                 0xff, 0xff, 0xff, 0xff}
        },
        {
            i3::DataProviderGeneratorI3v0::Pattern::Ascending,
            std::vector<uint8_t>{0x0, 0x0, 0x0, 0x0,
                                 0x1, 0x0, 0x0, 0x0,
                                 0x2, 0x0, 0x0, 0x0,
                                 0x3, 0x0, 0x0, 0x0,
                                 0x4, 0x0, 0x0, 0x0}
        },
    };

    expectedPayloads[i3::MessageI3v0_DataType::Float32] = Patterns_t{
        {
            i3::DataProviderGeneratorI3v0::Pattern::Zero,
            std::vector<uint8_t>{0x0, 0x0, 0x0, 0x0,
                                 0x0, 0x0, 0x0, 0x0,
                                 0x0, 0x0, 0x0, 0x0,
                                 0x0, 0x0, 0x0, 0x0,
                                 0x0, 0x0, 0x0, 0x0}
        },
        {
            i3::DataProviderGeneratorI3v0::Pattern::FF,
            std::vector<uint8_t>{0xff, 0xff, 0xff, 0xff,
                                 0xff, 0xff, 0xff, 0xff,
                                 0xff, 0xff, 0xff, 0xff,
                                 0xff, 0xff, 0xff, 0xff,
                                 0xff, 0xff, 0xff, 0xff}
        },
    };

    std::vector<uint8_t>& raw = expectedPayloads[i3::MessageI3v0_DataType::Float32]
                                                [i3::DataProviderGeneratorI3v0::Pattern::Ascending];
    raw.resize(sizeof(float) * kPayloadElementCount);

    for (int32_t i = 0; i < kPayloadElementCount; ++i)
    {
        const float v = static_cast<float>(i);
        memcpy(&raw[i * sizeof(float)], &v, sizeof(float));
    }

    for (auto datatype : dataTypes)
    {
        for (auto pattern : patterns)
        {
            std::vector<uint8_t> payloadExpected = expectedPayloads[datatype][pattern];

            const int32_t kBitrate = static_cast<int32_t>((sizeof(i3::MessageI3v0_Header) + i3::SizeOf(datatype) * kPayloadElementCount) * 8);
            constexpr decltype(i3::MessageI3v0_Header::SourceID) kSourceID = 42;

            i3::DataProviderGeneratorI3v0::Config cfg{};
            cfg.MetricBitrate = std::make_unique<i3::MetricGeneratorCBR>(kBitrate);
            cfg.SourceID = kSourceID;
            cfg.PayloadType = datatype;
            cfg.PayloadPattern = pattern;

            i3::DataProviderGeneratorI3v0 provider{std::move(cfg)};

            i3::Result<i3::types::bytespan, i3::DataProvider::ErrorCode> chunk0 = provider.read();
            EXPECT_EQ(chunk0.code(), i3::DataProvider::ErrorCode::EmptyChunk);

            i3::Clock::advance(1000ms);

            i3::Result<i3::types::bytespan, i3::DataProvider::ErrorCode> chunk = provider.read();

            EXPECT_TRUE(chunk.isOk());
            EXPECT_EQ(chunk->size(), sizeof(i3::MessageI3v0_Header) + i3::SizeOf(datatype) * kPayloadElementCount);

            i3::MessageI3v0 msg{};
            i3::ProtocolStatus status = msg.parse(*chunk);
            EXPECT_TRUE(status.isOk());

            EXPECT_EQ(msg.Header.SequenceNumber, 0);
            EXPECT_EQ(msg.Header.SourceID, kSourceID);
            EXPECT_EQ(msg.Header.PayloadType, datatype);
            EXPECT_EQ(msg.Header.PayloadSize, i3::SizeOf(datatype) * kPayloadElementCount);
            EXPECT_EQ(msg.Payload, payloadExpected);
        }
    }
}

TEST(DataProviderGeneratorI3v0, WeightDistributedMagicWord)
{
    using namespace std::chrono_literals;

    i3::Clock::reset();

    using MagicWord_t = decltype(i3::MessageI3v0_Header::MagicWord);

    constexpr decltype(i3::MessageI3v0_Header::SourceID) kSourceID = 42;
    constexpr i3::MessageI3v0_DataType kDatatype{i3::MessageI3v0_DataType::Int32};
    const int32_t kBitrate = static_cast<int32_t>((sizeof(i3::MessageI3v0_Header) + i3::SizeOf(kDatatype)) * 8);

    constexpr MagicWord_t kMagicWord0{i3::MessageI3v0_Header::kMagicWord};
    constexpr MagicWord_t kMagicWord1{'F', 'A', 'K', 'E'};

    i3::DataProviderGeneratorI3v0::Config::DistributionWeights<MagicWord_t> distribution{};
    distribution.add(0.5f, kMagicWord0);
    distribution.add(0.5f, kMagicWord1);

    i3::DataProviderGeneratorI3v0::Config cfg{};
    cfg.MetricBitrate = std::make_unique<i3::MetricGeneratorCBR>(kBitrate);
    cfg.MagicWord = distribution;
    cfg.SourceID = kSourceID;
    cfg.PayloadType = kDatatype;

    i3::DataProviderGeneratorI3v0 provider{std::move(cfg)};

    i3::Result<i3::types::bytespan, i3::DataProvider::ErrorCode> chunk0 = provider.read();
    EXPECT_EQ(chunk0.code(), i3::DataProvider::ErrorCode::EmptyChunk);

    std::map<MagicWord_t, int32_t> hist{};
    for (int32_t i = 0; i < 100; ++i)
    {
        i3::Clock::advance(1000ms);

        i3::Result<i3::types::bytespan, i3::DataProvider::ErrorCode> chunk = provider.read();

        EXPECT_TRUE(chunk.isOk());
        EXPECT_EQ(chunk->size(), sizeof(i3::MessageI3v0_Header) + i3::SizeOf(kDatatype));

        i3::MessageI3v0 msg{};
        i3::ProtocolStatus status = msg.parse(*chunk);
        if (status.isOk())
        {
            EXPECT_EQ(msg.Header.SequenceNumber, i);
            EXPECT_EQ(msg.Header.PayloadType, kDatatype);
            EXPECT_EQ(msg.Header.PayloadSize, i3::SizeOf(kDatatype));

            std::vector<uint8_t> payloadExpected{};
            payloadExpected.resize(i3::SizeOf(kDatatype), 0x0);

            EXPECT_EQ(msg.Payload, payloadExpected);

            hist[kMagicWord0]++;
        }
        else
        {
            EXPECT_EQ(status.code(), i3::ProtocolErrorCode::UnmatchedProtocol);
            hist[kMagicWord1]++;
        }
    }

    EXPECT_EQ(hist.size(), 2);
    EXPECT_TRUE(hist.count(kMagicWord0) > 0);
    EXPECT_TRUE(hist.count(kMagicWord1) > 0);
}

TEST(DataProviderGeneratorI3v0, WeightDistributedSourceID)
{
    using namespace std::chrono_literals;

    i3::Clock::reset();

    using SourceID_t = decltype(i3::MessageI3v0_Header::SourceID);

    constexpr i3::MessageI3v0_DataType kDatatype{i3::MessageI3v0_DataType::Int32};
    const int32_t kBitrate = static_cast<int32_t>((sizeof(i3::MessageI3v0_Header) + i3::SizeOf(kDatatype)) * 8);

    constexpr SourceID_t kSourceID0 = 42;
    constexpr SourceID_t kSourceID1 = -24;

    i3::DataProviderGeneratorI3v0::Config::DistributionWeights<SourceID_t> distribution{};
    distribution.add(0.5f, kSourceID0);
    distribution.add(0.5f, kSourceID1);

    i3::DataProviderGeneratorI3v0::Config cfg{};
    cfg.MetricBitrate = std::make_unique<i3::MetricGeneratorCBR>(kBitrate);
    cfg.SourceID = distribution;
    cfg.PayloadType = kDatatype;

    i3::DataProviderGeneratorI3v0 provider{std::move(cfg)};

    i3::Result<i3::types::bytespan, i3::DataProvider::ErrorCode> chunk0 = provider.read();
    EXPECT_EQ(chunk0.code(), i3::DataProvider::ErrorCode::EmptyChunk);

    std::map<SourceID_t, int32_t> hist{};
    for (int32_t i = 0; i < 100; ++i)
    {
        i3::Clock::advance(1000ms);

        i3::Result<i3::types::bytespan, i3::DataProvider::ErrorCode> chunk = provider.read();

        EXPECT_TRUE(chunk.isOk());
        EXPECT_EQ(chunk->size(), sizeof(i3::MessageI3v0_Header) + i3::SizeOf(kDatatype));

        i3::MessageI3v0 msg{};
        i3::ProtocolStatus status = msg.parse(*chunk);
        EXPECT_TRUE(status.isOk());

        EXPECT_EQ(msg.Header.SequenceNumber, i);
        EXPECT_EQ(msg.Header.PayloadType, kDatatype);
        EXPECT_EQ(msg.Header.PayloadSize, i3::SizeOf(kDatatype));

        std::vector<uint8_t> payloadExpected{};
        payloadExpected.resize(i3::SizeOf(kDatatype), 0x0);

        EXPECT_EQ(msg.Payload, payloadExpected);

        hist[msg.Header.SourceID]++;
    }

    EXPECT_EQ(hist.size(), 2);
    EXPECT_TRUE(hist.count(kSourceID0) > 0);
    EXPECT_TRUE(hist.count(kSourceID1) > 0);
}

TEST(DataProviderGeneratorI3v0, WeightDistributedPayloadType)
{
    using namespace std::chrono_literals;

    i3::Clock::reset();

    constexpr decltype(i3::MessageI3v0_Header::SourceID) kSourceID = 42;
    const int32_t kBitrate = static_cast<int32_t>((sizeof(i3::MessageI3v0_Header) + 1) * 8);

    constexpr i3::MessageI3v0_DataType kDataType0 = i3::MessageI3v0_DataType::UInt8;
    constexpr i3::MessageI3v0_DataType kDataType1 = i3::MessageI3v0_DataType::Float32;
    constexpr i3::MessageI3v0_DataType kDataType2 = static_cast<i3::MessageI3v0_DataType>(-42);

    i3::DataProviderGeneratorI3v0::Config::DistributionWeights<i3::MessageI3v0_DataType> distribution{};
    distribution.add(0.5f, kDataType0);
    distribution.add(0.5f, kDataType1);
    distribution.add(0.5f, kDataType2);

    i3::DataProviderGeneratorI3v0::Config cfg{};
    cfg.MetricBitrate = std::make_unique<i3::MetricGeneratorCBR>(kBitrate);
    cfg.SourceID = kSourceID;
    cfg.PayloadType = distribution;
    cfg.PayloadPattern = i3::DataProviderGeneratorI3v0::Pattern::Random;

    i3::DataProviderGeneratorI3v0 provider{std::move(cfg)};

    i3::Result<i3::types::bytespan, i3::DataProvider::ErrorCode> chunk0 = provider.read();
    EXPECT_EQ(chunk0.code(), i3::DataProvider::ErrorCode::EmptyChunk);

    std::map<i3::MessageI3v0_DataType, int32_t> hist{};
    for (int32_t i = 0; i < 100; ++i)
    {
        i3::Clock::advance(1000ms);

        i3::Result<i3::types::bytespan, i3::DataProvider::ErrorCode> chunk = provider.read();

        EXPECT_TRUE(chunk.isOk());

        i3::MessageI3v0 msg{};
        i3::ProtocolStatus status = msg.parse(*chunk);
        EXPECT_TRUE(status.isOk());

        EXPECT_EQ(msg.Header.SequenceNumber, i);
        EXPECT_EQ(msg.Header.SourceID, kSourceID);
        EXPECT_GT(msg.Header.PayloadSize, 0);
        EXPECT_GT(msg.Payload.size(), 0);

        hist[msg.Header.PayloadType]++;
    }

    EXPECT_EQ(hist.size(), 3);
    EXPECT_TRUE(hist.count(kDataType0) > 0);
    EXPECT_TRUE(hist.count(kDataType1) > 0);
    EXPECT_TRUE(hist.count(kDataType2) > 0);
}

TEST(DataProviderGeneratorI3v0, SequenceNumberSkipProbabilty)
{
    using namespace std::chrono_literals;

    i3::Clock::reset();

    constexpr i3::MessageI3v0_DataType kDatatype{i3::MessageI3v0_DataType::Int32};
    constexpr decltype(i3::MessageI3v0_Header::SourceID) kSourceID = 42;
    const int32_t kBitrate = static_cast<int32_t>((sizeof(i3::MessageI3v0_Header) + i3::SizeOf(kDatatype)) * 8);

    i3::DataProviderGeneratorI3v0::Config cfg{};
    cfg.MetricBitrate = std::make_unique<i3::MetricGeneratorCBR>(kBitrate);
    cfg.SourceID = kSourceID;
    cfg.PayloadType = kDatatype;
    cfg.SequenceNumberSkipProbabilty = 1.0f;

    i3::DataProviderGeneratorI3v0 provider{std::move(cfg)};

    i3::Result<i3::types::bytespan, i3::DataProvider::ErrorCode> chunk0 = provider.read();
    EXPECT_EQ(chunk0.code(), i3::DataProvider::ErrorCode::EmptyChunk);

    for (int32_t i = 0; i < 100; ++i)
    {
        i3::Clock::advance(1000ms);

        i3::Result<i3::types::bytespan, i3::DataProvider::ErrorCode> chunk = provider.read();

        EXPECT_TRUE(chunk.isOk());
        EXPECT_EQ(chunk->size(), sizeof(i3::MessageI3v0_Header) + i3::SizeOf(kDatatype));

        i3::MessageI3v0 msg{};
        i3::ProtocolStatus status = msg.parse(*chunk);
        EXPECT_TRUE(status.isOk());

        EXPECT_EQ(msg.Header.SequenceNumber, 2 * i + 1);
        EXPECT_EQ(msg.Header.PayloadType, kDatatype);
        EXPECT_EQ(msg.Header.PayloadSize, i3::SizeOf(kDatatype));

        std::vector<uint8_t> payloadExpected{};
        payloadExpected.resize(i3::SizeOf(kDatatype), 0x0);

        EXPECT_EQ(msg.Payload, payloadExpected);
    }
}
