#include "I3v0/DataProvider/DataProviderGeneratorI3v0.h"

#include "I3/Common/Types.h"

#include <random>

namespace
{

template<typename T>
constexpr T AlignUp(T value, T alignment)
{
    static_assert(std::is_integral_v<T>);

    return value + (-value & (alignment - 1));
}

static_assert(AlignUp(0, 1) == 0);
static_assert(AlignUp(0, 2) == 0);
static_assert(AlignUp(0, 8) == 0);

static_assert(AlignUp(1, 4) == 4);
static_assert(AlignUp(333, 4) == 336);

static_assert(AlignUp(5, 1) == 5);
static_assert(AlignUp(5, 2) == 6);
static_assert(AlignUp(5, 8) == 8);

struct Distribution
{
    template<typename T, std::enable_if_t<std::is_floating_point_v<T>, bool> = true>
    static auto& make()
    {
        static std::gamma_distribution<T> distribution{9.0f, 2.0f};
        return distribution;
    }

    template<typename T, std::enable_if_t<std::is_integral_v<T>, bool> = true>
    static auto& make()
    {
        static std::poisson_distribution<T> distribution{4};
        return distribution;
    }
};

template<typename T>
void PatternAscending(i3::types::span_mutable<T> dest)
{
    T* data = dest.data();
    for (int32_t i = 0; i < dest.size(); ++i)
    {
        *data++ = static_cast<T>(i);
    }
}

void PatternAscending(i3::types::bytespan_mutable dest, i3::MessageI3v0_DataType datatype)
{
    const int32_t sizeOfElement = IsValid(datatype) ? static_cast<int32_t>(i3::SizeOf(datatype)) : 1;
    i3LogAssert(sizeOfElement > 0);

    switch (datatype)
    {
    case i3::MessageI3v0_DataType::UInt8:
    {
        PatternAscending(i3::types::span_mutable<uint8_t>{reinterpret_cast<uint8_t*>(dest.data()), dest.size() / sizeOfElement});
        break;
    }
    case i3::MessageI3v0_DataType::Int16:
    {
        PatternAscending(i3::types::span_mutable<int16_t>{reinterpret_cast<int16_t*>(dest.data()), dest.size() / sizeOfElement});
        break;
    }
    case i3::MessageI3v0_DataType::Int32:
    {
        PatternAscending(i3::types::span_mutable<int32_t>{reinterpret_cast<int32_t*>(dest.data()), dest.size() / sizeOfElement});
        break;
    }
    case i3::MessageI3v0_DataType::Float32:
    {
        PatternAscending(i3::types::span_mutable<float>{reinterpret_cast<float*>(dest.data()), dest.size() / sizeOfElement});
        break;
    }
    }
}

template<typename T>
void PatternRandom(std::mt19937& generator, i3::types::span_mutable<T> dest)
{
    using U = std::conditional_t<!std::is_same_v<T, uint8_t>, T, uint16_t>;

    auto& distribution = Distribution::make<U>();

    T* data = dest.data();
    for (int32_t i = 0; i < dest.size(); ++i)
    {
        *data++ = static_cast<T>(distribution(generator));
    }
}

void PatternRandom(std::mt19937& generator, i3::types::bytespan_mutable dest, i3::MessageI3v0_DataType datatype)
{
    const int32_t sizeOfElement = IsValid(datatype) ? static_cast<int32_t>(i3::SizeOf(datatype)) : 1;
    i3LogAssert(sizeOfElement > 0);

    switch (datatype)
    {
    case i3::MessageI3v0_DataType::UInt8:
    {
        PatternRandom(generator, i3::types::span_mutable<uint8_t>{reinterpret_cast<uint8_t*>(dest.data()), dest.size() / sizeOfElement});
        break;
    }
    case i3::MessageI3v0_DataType::Int16:
    {
        PatternRandom(generator, i3::types::span_mutable<int16_t>{reinterpret_cast<int16_t*>(dest.data()), dest.size() / sizeOfElement});
        break;
    }
    case i3::MessageI3v0_DataType::Int32:
    {
        PatternRandom(generator, i3::types::span_mutable<int32_t>{reinterpret_cast<int32_t*>(dest.data()), dest.size() / sizeOfElement});
        break;
    }
    case i3::MessageI3v0_DataType::Float32:
    {
        PatternRandom(generator, i3::types::span_mutable<float>{reinterpret_cast<float*>(dest.data()), dest.size() / sizeOfElement});
        break;
    }
    }
}

} // namespace

namespace i3
{

class DataProviderGeneratorI3v0::Impl
{
public:
    DataProviderGeneratorI3v0::Impl(DataProviderGeneratorI3v0::Config cfg)
        : m_Config{std::move(cfg)}
        , m_RandomDevice{}
        , m_RandomGenerator{m_RandomDevice()}
    {
        if (auto* weights = std::get_if<1>(&m_Config.MagicWord))
        {
            m_DistributionMagicWord = std::discrete_distribution<int32_t>(weights->begin(), weights->end());
        }

        if (auto* weights = std::get_if<1>(&m_Config.SourceID))
        {
            m_DistributionSourceID = std::discrete_distribution<int32_t>(weights->begin(), weights->end());
        }

        if (auto* weights = std::get_if<1>(&m_Config.PayloadType))
        {
            m_DistributionPayloadType = std::discrete_distribution<int32_t>(weights->begin(), weights->end());
        }

        m_DistributionSequenceNumberSkip = std::bernoulli_distribution(m_Config.SequenceNumberSkipProbabilty);
    }

    void generate(std::vector<uint8_t>& dest, size_t size)
    {
        if (sizeof(MessageI3v0::Header) > size)
        {
            dest.resize(0);
            return;
        }

        const MessageI3v0_DataType payloadType = Get(m_Config.PayloadType, m_DistributionPayloadType);

        int32_t payloadSize = static_cast<int32_t>(size - sizeof(MessageI3v0::Header));
        const int32_t sizeOfElement = IsValid(payloadType) ? static_cast<int32_t>(SizeOf(payloadType)) : 1;
        payloadSize = AlignUp(payloadSize, sizeOfElement);
        i3LogAssert(payloadSize >= 0);

        if (m_DistributionSequenceNumberSkip(m_RandomGenerator))
        {
            ++m_SequenceNumberCurrent;
        }

        MessageI3v0 msg{};
        msg.Header.MagicWord = Get(m_Config.MagicWord, m_DistributionMagicWord);
        msg.Header.SequenceNumber = m_SequenceNumberCurrent++;
        msg.Header.SourceID = Get(m_Config.SourceID, m_DistributionSourceID);
        msg.Header.PayloadType = payloadType;
        msg.Header.PayloadSize = payloadSize;

        msg.Payload.resize(payloadSize);
        switch (m_Config.PayloadPattern)
        {
        case DataProviderGeneratorI3v0::Pattern::Zero:
        {
            memset(msg.Payload.data(), 0x0, payloadSize);
            break;
        }
        case DataProviderGeneratorI3v0::Pattern::FF:
        {
            memset(msg.Payload.data(), 0xff, payloadSize);
            break;
        }
        case DataProviderGeneratorI3v0::Pattern::Ascending:
        {
            types::bytespan_mutable data{msg.Payload.data(), msg.Payload.size()};
            PatternAscending(data, payloadType);

            break;
        }
        case DataProviderGeneratorI3v0::Pattern::Random:
        {
            types::bytespan_mutable data{msg.Payload.data(), msg.Payload.size()};
            PatternRandom(m_RandomGenerator, data, payloadType);

            break;
        }
        default:
        {
            i3LogError("Unexpected pattern has been provided=`%i`!", static_cast<int32_t>(m_Config.PayloadPattern));
            break;
        }
        }

        const ProtocolStatus status = msg.dump(dest);
        i3LogAssert(status.isOk());
    }

protected:
    template<typename T>
    T Get(Config::Variant<T>& var, std::discrete_distribution<int32_t>& distribution)
    {
        if (T* value = std::get_if<0>(&var))
        {
            return *value;
        }
        else
        {
            const int32_t generated = distribution(m_RandomGenerator);

            const Config::DistributionWeights<T>& weigths = std::get<1>(var);
            return weigths.valueOf(generated);
        }
    }

protected:
    DataProviderGeneratorI3v0::Config m_Config{};

    decltype(MessageI3v0_Header::SequenceNumber) m_SequenceNumberCurrent{};

    std::random_device m_RandomDevice{};
    std::mt19937 m_RandomGenerator{};

    std::discrete_distribution<int32_t> m_DistributionMagicWord{};
    std::discrete_distribution<int32_t> m_DistributionSourceID{};
    std::discrete_distribution<int32_t> m_DistributionPayloadType{};
    std::bernoulli_distribution m_DistributionSequenceNumberSkip{};
};

DataProviderGeneratorI3v0::DataProviderGeneratorI3v0(DataProviderGeneratorI3v0::Config cfg)
    : DataProviderGenerator{std::move(cfg.MetricBitrate), cfg.LiveTime, cfg.Balancer}
    , m_Impl{std::make_unique<DataProviderGeneratorI3v0::Impl>(std::move(cfg))}
{
}

DataProviderGeneratorI3v0::~DataProviderGeneratorI3v0() {}

void DataProviderGeneratorI3v0::generate(std::vector<uint8_t>& dest, size_t size)
{
    i3LogAssert_NonNull(m_Impl);

    m_Impl->generate(dest, size);
}

} // namespace i3
