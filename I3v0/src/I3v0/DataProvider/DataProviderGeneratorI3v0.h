#pragma once

#include "I3/DataProvider/DataProviderGenerator.h"

#include "I3v0/Message/MessageI3v0.h"

#include <variant>

namespace i3
{

class DataProviderGeneratorI3v0 : public DataProviderGenerator
{
public:
    enum class Pattern
    {
        Zero,
        FF,
        Ascending,
        Random
    };

    struct Config
    {
        template<typename T>
        class DistributionWeights
        {
        public:
            void add(double weight, T value);

            T valueOf(int32_t index) const;

            auto begin() const;
            auto end() const;

        protected:
            std::vector<double> m_Weights{};
            std::vector<T> m_Values{};
        };

        template<typename T>
        using Variant = std::variant<T, DistributionWeights<T>>;

        std::unique_ptr<IMetricGeneratorBitrate> MetricBitrate{};
        std::optional<std::chrono::milliseconds> LiveTime;
        std::optional<RateBalancer> Balancer;

        Variant<decltype(MessageI3v0_Header::MagicWord)> MagicWord{MessageI3v0_Header::kMagicWord};
        Variant<decltype(MessageI3v0_Header::SourceID)> SourceID{};
        Variant<decltype(MessageI3v0_Header::PayloadType)> PayloadType{};
        double SequenceNumberSkipProbabilty{};

        Pattern PayloadPattern{};
    };

public:
    DataProviderGeneratorI3v0(Config cfg);
    ~DataProviderGeneratorI3v0(); // For PImpl

protected:
    void generate(std::vector<uint8_t>& dest, size_t size) override;

protected:
    class Impl;
    std::unique_ptr<Impl> m_Impl{};
};

template<typename T>
void DataProviderGeneratorI3v0::Config::DistributionWeights<T>::add(double weight, T value)
{
    m_Weights.push_back(weight);
    m_Values.push_back(value);
}

template<typename T>
T DataProviderGeneratorI3v0::Config::DistributionWeights<T>::valueOf(int32_t index) const
{
    i3LogAssert(index >= 0 && index < m_Values.size());

    return m_Values[index];
}

template<typename T>
auto DataProviderGeneratorI3v0::Config::DistributionWeights<T>::begin() const
{
    return m_Weights.begin();
}

template<typename T>
auto DataProviderGeneratorI3v0::Config::DistributionWeights<T>::end() const
{
    return m_Weights.end();
}

} // namespace i3
