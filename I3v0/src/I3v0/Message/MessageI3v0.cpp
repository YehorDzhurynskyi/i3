#pragma once

#include "I3v0/Message/MessageI3v0.h"

#include "I3/Common/Log.h"

namespace i3
{

size_t SizeOf(MessageI3v0_DataType datatype)
{
    switch (datatype)
    {
    case MessageI3v0_DataType::UInt8: return 1;
    case MessageI3v0_DataType::Int16: return 2;
    case MessageI3v0_DataType::Int32: return 4;
    case MessageI3v0_DataType::Float32: return 4;
    }

    i3LogError("Unexpected Type=`%i`!", static_cast<int32_t>(datatype));

    return 0;
}

bool IsValid(MessageI3v0_DataType datatype)
{
    switch (datatype)
    {
    case MessageI3v0_DataType::UInt8:
    case MessageI3v0_DataType::Int16:
    case MessageI3v0_DataType::Int32:
    case MessageI3v0_DataType::Float32: return true;
    }

    return false;
}

Protocol MessageI3v0_Header::makeProtocol()
{
    Protocol protocol({'I', '3', 'v', '0'});

    protocol.add(kHashSequenceNumber, i3MPD_FIELD(MessageI3v0_Header, SequenceNumber));
    protocol.add(PredefinedHash_SourceID, i3MPD_FIELD(MessageI3v0_Header, SourceID));
    protocol.add(kHashPayloadType, i3MPD_FIELD(MessageI3v0_Header, PayloadType));

    protocol.addRepeated(i3CRC32_CompileTime(Payload),
        static_cast<int32_t>(sizeof(MessageI3v0_Header)),
        types::Type::UInt8,
        protocol.add(kHashPayloadSize, i3MPD_FIELD(MessageI3v0_Header, PayloadSize))
    );

    return protocol;
}

} // namespace i3
