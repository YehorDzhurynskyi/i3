#pragma once

#include "I3/Common/Result.h"

#include "I3/Message/Protocol.h"
#include "I3/Message/MessagePayloaded.h"

#include <vector>

namespace i3
{

enum class MessageI3v0_DataType : int8_t
{
    UInt8,
    Int16,
    Int32,
    Float32,
};

size_t SizeOf(MessageI3v0_DataType datatype);
bool IsValid(MessageI3v0_DataType datatype);

#pragma pack(push, 1)
struct MessageI3v0_Header
{
    constexpr static std::array<uint8_t, 4> kMagicWord{'I', '3', 'v', '0'};

    constexpr static crc32_t kHashSequenceNumber = i3CRC32_CompileTime(SequenceNumber);
    constexpr static crc32_t kHashPayloadSize = i3CRC32_CompileTime(PayloadSize);
    constexpr static crc32_t kHashPayloadType = i3CRC32_CompileTime(PayloadType);
    constexpr static crc32_t kHashPayload = i3CRC32_CompileTime(Payload);

    static Protocol makeProtocol();

    std::array<uint8_t, 4>  MagicWord{kMagicWord};
    int16_t                 SequenceNumber{};
    int8_t                  SourceID{};
    MessageI3v0_DataType    PayloadType{};
    int32_t                 PayloadSize{};
};
#pragma pack(pop)

static_assert(sizeof(MessageI3v0_Header) == 4 + 2 + 1 + 1 + 4);
static_assert(std::alignment_of_v<MessageI3v0_Header> == 1);

using MessageI3v0 = MessagePayloaded<MessageI3v0_Header, MessageI3v0_Header::kHashPayload>;

} // namespace i3
