#pragma once

#include "I3/DataSource/DataSourceMessage.h"
#include "I3v0/Message/MessageI3v0.h"

namespace i3
{

class DataSourceMessageI3v0 : public DataSourceMessage<MessageI3v0>
{
public:
    explicit DataSourceMessageI3v0(crc32_t sourceID);
    ~DataSourceMessageI3v0() override;

protected:
    size_t readInternal(types::bytespan_mutable dest) override;

    bool validateMessage(const MessageI3v0& msg) const override;

    void processMessageThread();
    types::bytespan processMessage(const MessageI3v0& msg);

protected:
    std::vector<uint8_t> m_Output{};
    decltype(MessageI3v0_Header::SequenceNumber) m_SequenceNumberLastProcessed{-1};


    Clock::duration m_NoMessageTimeout{std::chrono::seconds(1)};

    std::thread m_ProcessingWorker{};
    std::atomic_bool m_CancellationToken{};

    std::deque<uint8_t> m_Processed{};
    std::mutex m_ProcessedMutex{};
};

} // namespace i3
