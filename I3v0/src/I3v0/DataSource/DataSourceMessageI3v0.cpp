#include "I3v0/DataSource/DataSourceMessageI3v0.h"

#include "I3/Common/Profile.h"

#include <algorithm>

namespace
{

template<typename T>
void Fill(i3::types::bytespan_mutable dest, i3::types::span<T> src)
{
    i3LogAssert(dest.size() % sizeof(float) == 0);
    i3LogAssert((dest.size() / sizeof(float)) == src.size());

    if (src.size() == 0)
    {
        return;
    }

    auto [itMin, itMax] = std::minmax_element(src.begin(), src.end());

    i3LogAssert(itMin != src.end());
    i3LogAssert(itMax != src.end());

    const T minValue = *itMin;
    const T maxValue = *itMax;

    bool areAllSame = false;
    if constexpr (std::is_floating_point_v<T>)
    {
        areAllSame = std::abs(maxValue - minValue) < std::numeric_limits<T>::epsilon();
    }
    else
    {
        areAllSame = (maxValue - minValue == 0);
    }

    float* destPtr = reinterpret_cast<float*>(dest.data());
    const T* srcPtr = src.data();

    if (!areAllSame)
    {
        for (int32_t i = 0; i < src.size(); ++i)
        {
            *destPtr++ = (*srcPtr++ - minValue) / static_cast<float>(maxValue - minValue) * 2.0f - 1.0f;
        }
    }
    else
    {
        for (int32_t i = 0; i < src.size(); ++i)
        {
            *destPtr++ = 0.0f;
        }
    }
}

void FillInvalid(i3::types::bytespan_mutable dest)
{
    memset(dest.data(), 0x0, dest.size());
}

} // namespace

namespace i3
{

DataSourceMessageI3v0::DataSourceMessageI3v0(crc32_t sourceID)
    : DataSourceMessage<MessageI3v0>{sourceID}
{
    m_CancellationToken = false;
    m_ProcessingWorker = std::thread{&DataSourceMessageI3v0::processMessageThread, this};
}

DataSourceMessageI3v0::~DataSourceMessageI3v0()
{
    if (m_ProcessingWorker.joinable())
    {
        m_CancellationToken = true;
        m_QueueWriteUpdatedCV.notify_one();
        m_ProcessingWorker.join();
    }
}

size_t DataSourceMessageI3v0::readInternal(types::bytespan_mutable dest)
{
    i3ProfileFunction;

    size_t ret{};

    {
        i3ProfileBlock("WAIT LOCK");
        std::lock_guard lk(m_ProcessedMutex);
        i3ProfileBlockEnd;

        ret = std::min(m_Processed.size(), dest.size());

        i3ProfileBlock("COPY TO DEST");
        std::copy_n(m_Processed.begin(), ret, dest.begin());
        i3ProfileBlockEnd;

        i3ProfileBlock("REMOVE FROM DEQUE");
        m_Processed.erase(m_Processed.begin(), std::next(m_Processed.begin(), ret));
        i3ProfileBlockEnd;
    }

    return ret;
}

void DataSourceMessageI3v0::processMessageThread()
{
    i3ProfileFunction;

    while (!m_CancellationToken)
    {
        MessageI3v0 msg{};

        {
            i3ProfileBlock("DATA SOURCE WAIT");

            std::unique_lock lk(m_QueueWriteUpdatedMutex);

            const bool status = m_QueueWriteUpdatedCV.wait_for(lk,
                                                               m_NoMessageTimeout,
                                                               [this]()
            {
                return !m_QueueWrite.empty() || m_CancellationToken;
            });

            i3ProfileBlockEnd;

            if (m_CancellationToken)
            {
                break;
            }

            if (!status)
            {
                continue;
            }

            m_QueueWrite.front(msg);
            m_QueueWrite.pop();
        }

        types::bytespan processed = processMessage(msg);

        m_BitrateOut.add(processed.size());

        {
            i3ProfileBlock("WAIT LOCK");
            std::lock_guard lk(m_ProcessedMutex);
            i3ProfileBlockEnd;

            i3ProfileBlock("INSERT INTO DEQUE");
            m_Processed.insert(m_Processed.end(), processed.begin(), processed.end());
            i3ProfileBlockEnd;
        }
    }

    i3LogInfo("Shutdown DataSource=`0x%X` process-message worker thread successfully", getSourceID());
}

types::bytespan DataSourceMessageI3v0::processMessage(const MessageI3v0& msg)
{
    i3ProfileFunction;

    const size_t sizeElement = SizeOf(msg.Header.PayloadType);
    i3LogAssert(sizeElement > 0);

    const size_t sizeOutput = (msg.Header.PayloadSize / sizeElement) * sizeof(float);

    size_t sizeOutOfOrder = 0;
    const bool isInOrder = (msg.Header.SequenceNumber) == (m_SequenceNumberLastProcessed + 1);
    if (!isInOrder)
    {
        const decltype(MessageI3v0_Header::SequenceNumber) nOutOfOrder = msg.Header.SequenceNumber - (m_SequenceNumberLastProcessed + 1);
        sizeOutOfOrder = sizeOutput * std::abs(nOutOfOrder);
    }

    bool isValid = true;

    const bool isByteGranularityValid = (msg.Header.PayloadSize % sizeElement == 0);
    isValid &= isByteGranularityValid;

    m_Output.resize(sizeOutput + sizeOutOfOrder);

    FillInvalid(types::bytespan_mutable{m_Output.data(), sizeOutOfOrder});

    types::bytespan_mutable dest{m_Output.data() + sizeOutOfOrder, sizeOutput};
    if (isValid)
    {
        switch (msg.Header.PayloadType)
        {
        case MessageI3v0_DataType::UInt8:
        {
            Fill(dest, types::span<uint8_t>{reinterpret_cast<const uint8_t*>(msg.Payload.data()), msg.Header.PayloadSize / sizeof(uint8_t)});
            break;
        }
        case MessageI3v0_DataType::Int16:
        {
            Fill(dest, types::span<int16_t>{reinterpret_cast<const int16_t*>(msg.Payload.data()), msg.Header.PayloadSize / sizeof(int16_t)});
            break;
        }
        case MessageI3v0_DataType::Int32:
        {
            Fill(dest, types::span<int32_t>{reinterpret_cast<const int32_t*>(msg.Payload.data()), msg.Header.PayloadSize / sizeof(int32_t)});
            break;
        }
        case MessageI3v0_DataType::Float32:
        {
            Fill(dest, types::span<float>{reinterpret_cast<const float*>(msg.Payload.data()), msg.Header.PayloadSize / sizeof(float)});
            break;
        }
        default:
        {
            i3LogError("Unexpected Payload Type=`%i`!", static_cast<int32_t>(msg.Header.PayloadType));
            break;
        }
        }
    }
    else
    {
        FillInvalid(dest);
    }

    m_SequenceNumberLastProcessed = msg.Header.SequenceNumber;

    return types::bytespan{m_Output.data(), m_Output.size()};
}

bool DataSourceMessageI3v0::validateMessage(const MessageI3v0& msg) const
{
    const bool isPayloadTypeValid = IsValid(msg.Header.PayloadType);
    if (!isPayloadTypeValid)
    {
        return false;
    }

    return true;
}

} // namespace i3
